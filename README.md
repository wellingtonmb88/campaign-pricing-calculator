# campaing princing calculator

## Project overview

For this project, I built a web application to calculate the cost a campaing. My app allow admins/support user to manager the pricing for Distribution, Printing, Industry and Cities where the campaing will be released.

## TL;DR

To get started:
- Server
  - First install all project dependencies:
      - cd backend
      - npm install
  - Run the Server locally:
      - npm run install-local-db
      - npm run migrate-local-db
      - npm start-server-local
  - Run the Tests:
      - npm run test
  - Run the Test Coverage:
      - npm run coverage

- WebSite
  - First install all project dependencies:
      - cd frontend
      - npm install
  - Run the WebSite (* You need to start the Server first): 
      - npm start or  yearn start

## Project Web Status:
- [X] Login
- [ ] User Session
- [X] City CRUD
- [ ] Industry CRUD
- [ ] Printing CRUD
- [X] Cost per Acquisition 
- [X] Number of acquisitions
- [X] Map City Selection
- [ ] Loading indicator
- [ ] Error Handler Messages

## Technologies:
  - AWS Lambda
  - DynamoDB
  - Serverless [Serverless Framework] (https://serverless.com/)
  - Typescript
  - React
  - React Context API
  - Google Maps
  - GraphQl [Apollo] (https://www.apollographql.com/docs/apollo-server/)
  - Material-UI (https://material-ui-next.com/)
 