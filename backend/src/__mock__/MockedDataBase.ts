
import { IDataBase } from "../repository/IDataBase";
import { BaseModel } from "../model/BaseModel";

export class MockedDataBase implements IDataBase<BaseModel> {

    shouldShowError = false;

    constructor(private collection: any[]) {
    }

    setupError = (showError) => {
        this.shouldShowError = showError;
    }

    async getAll(page: number, limit: number, params?: any): Promise<{ error: any; data: any; }> {
        return new Promise<{ error: boolean, data: any }>(async (resolve, reject) => {
            if (this.shouldShowError) {
                resolve({
                    error: true, data: null,
                });
            } else {
                resolve({
                    error: false, data: {
                        page,
                        count: this.collection.length,
                        items: this.collection,
                    },
                });
            }
        });
    }

    async get(key: string, value: any, params?: any): Promise<{ error: any; data: any; }> {
        return new Promise<{ error: boolean, data: any }>(async (resolve, reject) => {
            const item = this.collection.filter(u => u[key] === value)[0];
            if (item) {
                resolve({ error: false, data: item });
            } else {
                resolve({ error: true, data: null });
            }
        });
    }

    async save(data: BaseModel, params?: any): Promise<{ error: any; success: any; }> {
        return new Promise<{ error: boolean, success: any }>(async (resolve, reject) => {
            if (this.shouldShowError) {
                resolve({ error: true, success: false });
            } else {
                resolve({ error: false, success: true });
            }
        });
    }

    async update(data: BaseModel, params?: any): Promise<{ error: any; success: any; }> {
        return new Promise<{ error: boolean, success: any }>(async (resolve, reject) => {
            if (this.shouldShowError) {
                resolve({ error: true, success: false });
            } else {
                resolve({ error: false, success: true });
            }
        });
    }

    async delete(data: BaseModel, params?: any): Promise<{ error: any; success: any; }> {
        return new Promise<{ error: boolean, success: any }>(async (resolve, reject) => {
            if (this.shouldShowError) {
                resolve({ error: true, success: false });
            } else {
                resolve({ error: false, success: true });
            }
        });
    }

}
