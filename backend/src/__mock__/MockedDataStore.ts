
import { IDataSource } from "../repository/IDataSource";
import { User } from "../model/User";
import * as credentialsHandler from "../utils/credentials-handler";

const uuid = require("uuid").v1;

export class MockedDataStore implements IDataSource<User> {

    constructor(private collection: any[]) {
    }

    async getAll(page: number, limit: number): Promise<{ error: boolean; data: any[]; }> {
        return new Promise<{ error: boolean, data: any[] }>(async (resolve, reject) => {
            resolve({ error: false, data: this.collection });
        });
    }

    async get(key: string, value: any): Promise<{ error: boolean; data: any; }> {
        return new Promise<{ error: boolean, data: any }>(async (resolve, reject) => {
            const item = this.collection.filter(u => u[key] === value)[0];
            if (item) {
                resolve({ error: false, data: item });
            } else {
                resolve({ error: true, data: null });
            }
        });
    }

    async remove(data: any): Promise<{ error: boolean; data: any; }> {
        return new Promise<{ error: boolean, data: any }>(async (resolve, reject) => {
            const item = this.collection.filter(i => i.id === data.id)[0];
            if (item) {
                item.deleted = true;
                resolve({ error: false, data: item });
            } else {
                resolve({ error: true, data: null });
            }
        });
    }

    async update(data: any): Promise<{ error: boolean; data: any; }> {
        return new Promise<{ error: boolean, data: any }>(async (resolve, reject) => {
            const item = this.collection.filter(i => i.id === data.id)[0];
            if (item) {
                item.updated = true;
                if (data.password) {
                    item.password = await credentialsHandler.hashPassword(data.password);
                }
                resolve({ error: false, data: item });
            } else {
                resolve({ error: true, data: null });
            }
        });
    }
    async create(data: any, shouldHashPwd?: boolean): Promise<{ error: boolean; data: any; }> {
        return new Promise<{ error: boolean, data: any }>(async (resolve, reject) => {
            if (data) {
                data.id = uuid();
                data.createdAt = new Date().getTime();
                if (data.password) {
                    data.password = await credentialsHandler.hashPassword(data.password);
                }
                this.collection.push(data);
                resolve({ error: false, data: this.collection });
            } else {
                resolve({ error: true, data: null });
            }
        });
    }
}
