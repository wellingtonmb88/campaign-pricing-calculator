
export interface IDataBase<T> {

    getAll(page: number, limit: number, params?: any): Promise<{ error, data }>;

    get(key: string, value: any, params?: any): Promise<{ error, data }>;

    save(data: T, params?: any): Promise<{ error, success }>;

    update(data: T, params?: any): Promise<{ error, success }>;

    delete(data: T, params?: any): Promise<{ error, success }>;
}
