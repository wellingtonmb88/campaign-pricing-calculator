
import { BaseModel } from "../model/BaseModel";
import { IDataBase } from "./IDataBase";

let dynamoDb = require("serverless-dynamodb-client").doc;

export const initializateDynamoClient = (newDynamo) => {
    dynamoDb = newDynamo;
};

/** Class representing a dynamoDb manager */
export class DynamoDbManager implements IDataBase<BaseModel> {

    dataWithKey: any;

    /**
     * Retrieve all items by page and limit from db.
     * @param page - The page number to retrieve items.
     * @param limit  - The number of items to return.
     * @param params - Parameters to be passed into dynamoDB.
     */
    async getAll(page: number, limit: number, params?: any): Promise<{ error: any; data: any; }> {
        if (params == null || params === undefined) {
            throw new Error("params property is required.");
        }
        if (page < 1) {
            page = 1;
        }
        if (limit < 1) {
            limit = 5;
        }
        const dataWithKey = {
            result: [],
        };
        const newParams = {
            Limit: limit,
            TableName: params.TableName,
        };
        return new Promise<{ error: any, data: any }>(async (resolve, reject) => {
            this.onScan(dataWithKey, newParams, (error, result) => {
                if (error || result === null) {
                    resolve({ error: true, data: null });
                } else {
                    const startIndex = (page - 1) * limit;
                    const endIndex = startIndex + limit;
                    const items = result.slice(startIndex, endIndex);
                    resolve({
                        data: {
                            count: items.length,
                            items,
                            page,
                        },
                        error: false,
                    });
                }
            });
        });
    }

    /**
     * Retrieve a item from db.
     * @param key - The key that represents the database"s column name.
     * @param value - The value to find the in database.
     * @param params - Parameters to be passed into dynamoDB.
     */
    async get(key: string, value: any, params?: any): Promise<{ error: any; data: any; }> {
        if (params == null || params === undefined) {
            throw new Error("params property is required.");
        }

        if (key === "id") {
            const paramsUpdate = {
                ExpressionAttributeNames: JSON.parse(`{"#k":"${key}"}`),
                ExpressionAttributeValues: { ":v": value },
                KeyConditionExpression: "#k = :v",
                TableName: params.TableName,
            };
            return new Promise<{ error, data }>(async (resolve, reject) => {
                return dynamoDb.query(paramsUpdate, (error, result) => {
                    if (error) {
                        resolve({ error: true, data: null });
                    } else {
                        const item = result.Items[0];
                        if (item) {
                            resolve({ error: false, data: item });
                        } else {
                            resolve({ error: false, data: {} });
                        }
                    }
                });
            });
        } else {
            return new Promise<{ error, data }>(async (resolve, reject) => {
                return dynamoDb.scan(params, (error, result) => {
                    if (error) {
                        resolve({ error: true, data: null });
                    } else {
                        const item = result.Items.filter(i => i[key] === value)[0];
                        if (item) {
                            resolve({ error: false, data: item });
                        } else {
                            resolve({ error: false, data: {} });
                        }
                    }
                });
            });
        }
    }

    /**
     * Save a item in the db.
     * @param item  - Object that represents an user model.
     * @param params - Parameters to be passed into dynamoDB.
     */
    async save(item: BaseModel, params?: any): Promise<{ error, success }> {
        if (params == null || params === undefined) {
            throw new Error("params property is required.");
        }
        const paramsUpdate = {
            Item: item,
            TableName: params.TableName,
        };
        return new Promise<{ error, success }>((resolve, reject) => {
            return dynamoDb.put(paramsUpdate, (error, result) => {
                if (error) {
                    resolve({ error: true, success: false });
                } else {
                    resolve({ error: false, success: true });
                }
            });
        });
    }

    /**
     * Update a item in the db.
     * @param item  - Object that represents an user model.
     * @param params - Parameters to be passed into dynamoDB.
     */
    async update(item: BaseModel, params?: any): Promise<{ error: any; success: any; }> {
        if (params == null || params === undefined) {
            throw new Error("params property is required.");
        }
        return new Promise<{ error, success }>(async (resolve, reject) => {
            return dynamoDb.update(params, (error, result) => {
                if (error) {
                    resolve({ error: true, success: false });
                } else {
                    resolve({ error: false, success: true });
                }
            });
        });
    }

    /**
     * Delete a item from db.
     * @param item - Object that represents an user model.
     * @param params - Parameters to be passed into dynamoDB.
     */
    async delete(item: BaseModel, params?: any): Promise<{ error: any; success: any; }> {
        if (params == null || params === undefined) {
            throw new Error("params property is required.");
        }
        const paramsUpdate = {
            Key: {
                id: item.id,
            },
            TableName: params.TableName,
        };
        return new Promise<{ error, success }>((resolve, reject) => {
            return dynamoDb.delete(paramsUpdate, (error, result) => {
                if (error) {
                    resolve({ error: true, success: false });
                } else {
                    resolve({ error: false, success: true });
                }
            });
        });
    }

    /**
     * Scan into database until to find the last result.
     * @param dataWithKey - Variable to concat the result of each search.
     * @param params - Parameters to be passed into dynamoDB.
     * @param callback - Callback when the seeking is done.
     */
    private onScan(dataWithKey: any, params: any, callback: (error: boolean, data: any) => void) {

        dynamoDb.scan(params, (error, result) => {
            if (error) {
                callback(true, null);
                return;
            }
            dataWithKey.result = dataWithKey.result.concat(result.Items);
            if (typeof result.LastEvaluatedKey === "undefined") {
                // pagination done, this is the last page as LastEvaluatedKey is undefined
                callback(false, dataWithKey.result);
                return;
            } else {
                // keep pagination for more data.
                params.ExclusiveStartKey = result.LastEvaluatedKey;
                this.onScan(dataWithKey, params, callback);
            }

        });
    }
}
