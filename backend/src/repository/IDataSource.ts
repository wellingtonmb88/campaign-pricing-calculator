
export interface IDataSource<T> {

    getAll(page: number, limit: number): Promise<{ error: boolean, data: T[] }>;

    get(key: string, value: any): Promise<{ error: boolean, data: T }>;

    remove(data: T): Promise<{ error: boolean, data: T }>;

    update(data: T): Promise<{ error: boolean, data: T }>;

    create(data: T, shouldHashPwd?: boolean): Promise<{ error: boolean, data: T }>;
}
