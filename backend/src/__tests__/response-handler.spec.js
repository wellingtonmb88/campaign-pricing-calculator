
const expect = require('chai').expect;
const responseHandler = require('../middleware/response-handler').responseHandler;

describe('response-handler functions', () => {

    it('should check response successfully', async () => {
        const ctx = {
        };
        await responseHandler(ctx, (s) => { });
        expect(ctx.status).to.be.undefined;
        expect(ctx.body).to.be.undefined;
        expect(ctx.type).to.be.equal('application/json');
    });

    it('should check response unsuccessfully', async () => {
        const ctx = {
        };
        await responseHandler(ctx, null);
        expect(ctx.status).to.be.equal(500);
        expect(ctx.body).to.be.equal('{"error":"next is not a function"}');
    });
});