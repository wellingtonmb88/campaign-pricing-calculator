

const expect = require('chai').expect;

const __id = 'aujiaikoak9903903-3093';
const _username = 'email';

const user = {
  _id: __id,
  username: _username,
  roles: "admin"
};

const AUDIENCES = [
  {
    id: "108202",
    username: "username",
    email: "email@gmail.com",
    role: "admin",
    deleted: false,
    createdAt: 0,
    password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
  },
  {
    id: "0190383",
    username: "username2",
    email: "email2@gmail.com",
    role: "support",
    deleted: false,
    createdAt: 1,
    password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
  }
];

const distribution = {
  fliersPerMission: 500,
  costPerMission: 0.15
};

const printing = {
  orders: 6000,
  rate: 0.35
};

describe('handler calculations api', () => {
  let handler;
  let token;
  let routes;
  let tokenHandler;

  beforeEach(() => {
    delete process.env.JWT_SECRET_KEY;
    process.env.JWT_SECRET_KEY = 'JWT_SECRET_KEY-TEST';

    routes = require('../../api/calculations/routers');

    handler = require('../../../handler');
    tokenHandler = require('../../utils/token-handler');
    token = tokenHandler.createToken(user);
  });

  describe('calculate campaing', () => {
    it('calculate campaing successfully', async () => {
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        console.log('response', response)
        console.log('error', error)
        expect(response.statusCode).to.be.equal(200);
        expect(response.body)
          .to.be.equal('{"data":{"calculate":{"totalCampaign":2104.5,"costPerAcquisition":28.06,"numberOfAcquisitions":75}}}');
      };
      await handler.calculations({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `{
            calculate(
              cities:[{
                distribution: {
                  fliersPerMission: 500,
                  costPerMission: 0.15
                },
                purpose: 15000
            }],
            printing: {
              orders: 6000,
              rate: 0.35
            },
            conversionRate: 0.50
            ){
              totalCampaign,
              costPerAcquisition,
              numberOfAcquisitions
            }
          }`
        },
        path: "/calculations/graphql"
      }, context, callback);
    });
  });
});