
const expect = require("chai").expect;

const __id = "aujiaikoak9903903-3093";
const _username = "username";

const user = {
    _id: __id,
    username: _username,
    roles: "admin"
};

describe("validate token JWT", () => {
    let tokenHandler;

    beforeEach(() => {
        delete process.env.JWT_SECRET_KEY;
        process.env.JWT_SECRET_KEY = "JWT_SECRET_KEY-TEST";
        tokenHandler = require("../utils/token-handler");
    });

    it("should create token successfully", () => {
        const token = tokenHandler.createToken(user)
        expect(token).not.to.be.null;
    });

    it("should create token for admin successfully", () => {
        user.roles = "admin";
        const token = tokenHandler.createToken(user)
        expect(token).not.to.be.null;

        const tokenDecoded = tokenHandler.decodeToken(token);
        expect(tokenDecoded).not.to.be.null;
        expect(tokenDecoded.scope).to.be.equal("admin");
    });

    it("should create token for common successfully", () => {
        user.roles = "common";
        const token = tokenHandler.createToken(user)
        expect(token).not.to.be.null;

        const tokenDecoded = tokenHandler.decodeToken(token);
        expect(tokenDecoded).not.to.be.null;
        expect(tokenDecoded.scope).to.be.equal("common");
    });

    it("should create token for support successfully", () => {
        user.roles = "support";
        const token = tokenHandler.createToken(user)
        expect(token).not.to.be.null;

        const tokenDecoded = tokenHandler.decodeToken(token);
        expect(tokenDecoded).not.to.be.null;
        expect(tokenDecoded.scope).to.be.equal("support");
    });

    it("should create token without scope successfully", () => {
        user.roles = [];
        const token = tokenHandler.createToken(user)
        expect(token).not.to.be.null;

        const tokenDecoded = tokenHandler.decodeToken(token);
        expect(tokenDecoded).not.to.be.null;
        expect(tokenDecoded.scope).to.be.empty;
    });

    it("should decode token successfully", () => {
        user.roles = "admin";
        const token = tokenHandler.createToken(user);
        expect(token).not.to.be.null;

        const tokenDecoded = tokenHandler.decodeToken(token);
        expect(tokenDecoded).not.to.be.null;
        expect(tokenDecoded.id).to.be.equal(__id);
        expect(tokenDecoded.username).to.be.equal(_username);
        expect(tokenDecoded.scope).to.be.equal("admin");
    });

});