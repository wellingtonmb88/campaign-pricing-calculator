
const expect = require("chai").expect;
const assert = require("chai").assert;
const MockedDataStore = require("../__mock__/MockedDataStore").MockedDataStore;

const __id = "aujiaikoak9903903-3093";
const _username = "username";

const user = {
    _id: __id,
    username: _username,
    roles: "admin"
};

const AUDIENCES = [
    {
        id: "108202",
        username: "username",
        email: "email@gmail.com",
        deleted: false,
        createdAt: 0,
        password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
    },
    {
        id: "0190383",
        username: "username2",
        email: "email2@gmail.com",
        deleted: false,
        createdAt: 1,
        password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
    }
];

const dataStore = new MockedDataStore(AUDIENCES);

describe("authorizer middleware functions", () => {
    let authorizer;
    let tokenHandler;

    beforeEach(() => {
        delete process.env.JWT_SECRET_KEY;
        process.env.JWT_SECRET_KEY = "JWT_SECRET_KEY-TEST";
        tokenHandler = require("../utils/token-handler");
        authorizer = require("../middleware/authorizer");
    });

    it("should check authorization scope for admin, support and common successfully", async () => {
        const token = tokenHandler.createToken(user)
        await authorizer.authScope({ admin: true, support: true, common: true })({
            request: {
                header: {
                    authorization: `Bearer ${token}`
                }
            },
            throw: (msg) => { assert.fail(); }
        }, (s) => { expect(s).to.be.undefined; });
    });

    it("should check authorization missing bearer successfully", async () => {
        const token = tokenHandler.createToken(user)
        await authorizer.authScope({ admin: true, support: true, common: true })({
            request: {
                header: {
                    authorization: null
                }
            },
            throw: (msg) => { expect(msg.toString()).to.be.equal("Error: Bearer must be provided!"); }
        }, (s) => { assert.fail(); });
    });

    it("should check authorization missing token successfully", async () => {
        const token = tokenHandler.createToken(user)
        await authorizer.authScope({ admin: true, support: true, common: true })({
            request: {
                header: {
                    authorization: "Bearer "
                }
            },
            throw: (msg) => { expect(msg.toString()).to.be.equal("Error: Token must be provided!"); }
        }, (s) => { assert.fail(); });
    });

    it("should check unauthorized token successfully", async () => {
        const token = tokenHandler.createToken(user)
        await authorizer.authScope({ admin: true, support: true, common: true })({
            request: {
                header: {
                    authorization: "Bearer invalid-token-invalid"
                }
            },
            throw: (msg) => { expect(msg.toString()).to.be.equal("Error: jwt malformed"); }
        }, (s) => { assert.fail(); });
    });

    it("should check authorization missing admin scope successfully", async () => {
        user.roles = "common";
        const token = tokenHandler.createToken(user)
        await authorizer.authScope({ admin: true })({
            request: {
                header: {
                    authorization: `Bearer ${token}`
                }
            },
            throw: (msg) => { expect(msg.toString()).to.be.equal("Error: Admin scope is required."); }
        }, (s) => { assert.fail(); });
    });

    it("should check authorization missing support scope successfully", async () => {
        user.roles = "admin";
        const token = tokenHandler.createToken(user)
        await authorizer.authScope({ support: true })({
            request: {
                header: {
                    authorization: `Bearer ${token}`
                }
            },
            throw: (msg) => { expect(msg.toString()).to.be.equal("Error: Support scope is required."); }
        }, (s) => { assert.fail(); });
    });

    it("should check authorization missing common scope successfully", async () => {
        user.roles = "admin";
        const token = tokenHandler.createToken(user)
        await authorizer.authScope({ common: true })({
            request: {
                header: {
                    authorization: `Bearer ${token}`
                }
            },
            throw: (msg) => { expect(msg.toString()).to.be.equal("Error: Common scope is required."); }
        }, (s) => { assert.fail(); });
    });

    it("should check authorization missing admin, support and common scopes successfully", async () => {
        user.roles = "-invalid-";
        const token = tokenHandler.createToken(user)
        await authorizer.authScope({ common: true, admin: true, support: true })({
            request: {
                header: {
                    authorization: `Bearer ${token}`
                }
            },
            throw: (msg) => { expect(msg.toString()).to.be.equal("Error: Admin or Support or Common scope is required."); }
        }, (s) => { assert.fail(); });
    });

    it("should authorize default credentials successfully", async () => {
        const token = tokenHandler.createToken(user)
        const result = await authorizer
            .authorizeDefaultCredentials(dataStore, "password", user.username);
        expect(result.status).to.be.equal(202);
    });
});