const expect = require('chai').expect;
const MockedDataBase = require('../../__mock__/MockedDataBase').MockedDataBase;

const CITIES = [
    {
      id: "108202",
      name: "city_name",
      distribution: {
        fliersPerMission: 600,
        costPerMission: 0.16
      },
      purpose: {
        test: 15000,
        validate: 20000,
        growth: 25000,
        scaleUp: 30000
      },
      latitude: -1099202.010,
      longitude: -8480380.010,
      deleted: false,
      createdAt: 0,
    },
    {
      id: "0190383",
      name: "city_name2",
      distribution: {
        fliersPerMission: 500,
        costPerMission: 0.15
      },
      purpose: {
        test: 13000,
        validate: 23000,
        growth: 28000,
        scaleUp: 33000
      },
      latitude: -1099202.010,
      longitude: -8480380.010,
      deleted: false,
      createdAt: 1,
    }
  ];

describe('CityDataStore function', () => {
    let CityDataStore;
    let dataStore;

    beforeEach(() => {
        CityDataStore = require('../../api/cities/repository/CityDataStore').CityDataStore;
        mockedDataBase = new MockedDataBase(CITIES);
        mockedDataBase.setupError(false);
        dataStore = new CityDataStore(mockedDataBase);
    });

    describe('getAll', () => {
        it('should get all cities successfully', async () => {
            const result = await dataStore.getAll(1, 3);
            expect(result.data.items.length).to.be.equal(2);
        });

        it('should get all cities unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.getAll(1, 3);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('get', () => {
        it('should get city successfully', async () => {
            const result = await dataStore.get('id', CITIES[0].id);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(CITIES[0].id);
        });

        it('should get city unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.get('id', 'invalid-id');
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('create', () => {
        it('should create city successfully', async () => {
            const result = await dataStore.create(CITIES[0], false);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(CITIES[0].id);
        });

        it('should create city unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.create(CITIES[0], false);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('update', () => {
        it('should update city successfully', async () => {
            const result = await dataStore.update(CITIES[0]);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(CITIES[0].id);
        });

        it('should update city unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.update(CITIES[0]);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('delete', () => {
        it('should delete city successfully', async () => {
            const result = await dataStore.remove(CITIES[0]);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(CITIES[0].id);
        });

        it('should delete city unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.remove(CITIES[0]);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

});