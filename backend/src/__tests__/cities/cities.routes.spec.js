

const expect = require('chai').expect;
const MockedDataBase = require('../../__mock__/MockedDataBase').MockedDataBase;

const __id = 'aujiaikoak9903903-3093';
const _username = 'email';

const user = {
  _id: __id,
  username: _username,
  roles: "admin"
};

const CITIES = [
  {
    id: "108202",
    name: "city_name",
    distribution: {
      fliersPerMission: 600,
      costPerMission: 0.16
    },
    purpose: {
      test: 15000,
      validate: 20000,
      growth: 25000,
      scaleUp: 30000
    },
    latitude: -1099202.010,
    longitude: -8480380.010,
    deleted: false,
    createdAt: 0,
  },
  {
    id: "0190383",
    name: "city_name2",
    distribution: {
      fliersPerMission: 500,
      costPerMission: 0.15
    },
    purpose: {
      test: 13000,
      validate: 23000,
      growth: 28000,
      scaleUp: 33000
    },
    latitude: -1099202.010,
    longitude: -8480380.010,
    deleted: false,
    createdAt: 1,
  }
];

describe('handler cities api', () => {
  let handler;
  let token;
  let routes;
  let tokenHandler;
  let mockedDataBase;

  beforeEach(() => {
    delete process.env.JWT_SECRET_KEY;
    delete process.env.TABLE_CITY;
    process.env.JWT_SECRET_KEY = 'JWT_SECRET_KEY-TEST';
    process.env.TABLE_CITY = 'dev-table-city';

    routes = require('../../api/cities/routers');

    let cities = JSON.parse(JSON.stringify(CITIES));
    mockedDataBase = new MockedDataBase(cities);
    routes.setupTest(mockedDataBase);

    handler = require('../../../handler');
    tokenHandler = require('../../utils/token-handler');
    token = tokenHandler.createToken(user);
  });

  describe('getAll', () => {
    it('should fetch cities list successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"cities":{"items":[{"id":"108202","name":"city_name"},{"id":"0190383","name":"city_name2"}]}}}')
          .to.be.equal(response.body);
      };
      await handler.cities({
        headers: {
          authorization: `Bearer ${token}`
        },
        httpMethod: "GET",
        body: "",
        path: "/cities/graphql",
        "queryStringParameters": {
          "query": "{cities(page:1, limit:10){items{id name}}}"
        }
      }, context, callback);
    });

    it('should fetch cities list unsuccessfully', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect('Could not fetch city\'s list!')
          .to.be.equal(JSON.parse(response.body).errors[0].message);
      };
      await handler.cities({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/cities/graphql",
        "queryStringParameters": {
          "query": "{cities(page:1, limit:10){items{id name}}}"
        }
      }, context, callback);
    });
  });

  describe('get', () => {
    it('should find city by id successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"city":{"id":"108202","name":"city_name"}}}')
          .to.be.equal(response.body);
      };
      await handler.cities({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/cities/graphql",
        "queryStringParameters": {
          "query": '{city(id:"108202"){id name}}'
        }
      }, context, callback);
    });

    it('should find city by id unsuccessfully', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('City not found!');
      };
      await handler.cities({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/cities/graphql",
        "queryStringParameters": {
          "query": '{city(id:"108202-Invalid"){id name}}'
        }
      }, context, callback);
    });
  });

  describe('delete', () => {

    it('should delete city successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"deleteCity":{"id":"108202","name":"city_name"}}}')
          .to.be.equal(response.body);
      };
      await handler.cities({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": 'mutation{deleteCity(id:"108202"){id name}}'
        },
        path: "/cities/graphql"
      }, context, callback);
    });

    it('should not delete city not find', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect('City not found!')
          .to.be.equal(JSON.parse(response.body).errors[0].message);
      };
      await handler.cities({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": 'mutation{deleteCity(id:"108202-Invalid"){id name deleted}}'
        },
        path: "/cities/graphql"
      }, context, callback);
    });

  });

  describe('update', () => {

    it('should update city successfully', (done) => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"updateCity":{"id":"108202","name":"name_update"}}}')
          .to.be.equal(response.body);
        done();
      };
      handler.cities({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{updateCity(city:{id:"108202", name:"name_update"})
            {id name}}`
        },
        path: "/cities/graphql"
      }, context, callback);
    });

    it('should update city unsuccessfully', (done) => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = async (error, response) => {
        expect(response.statusCode).to.be.equal(400);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('City could not be updated!');
        done();
      };
      handler.cities({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{updateCity(city:
            {id:"108202", name:"name_update"})
            {id name deleted}}`
        },
        path: "/cities/graphql"
      }, context, callback);
    });

  });

  describe('create', () => {
    it('should create city successfully', (done) => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"createCity":{"name":"city_name","deleted":false}}}')
          .to.be.equal(response.body);
        done();
      };
      handler.cities({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{createCity(newCity:
            {name:"city_name", 
                distribution:{
                  fliersPerMission: 600,
                  costPerMission: 0.16
                }, 
                purpose: {
                  test: 15000,
                  validate: 20000,
                  growth: 25000,
                  scaleUp: 30000
                },
                latitude: -1099202.010,
                longitude: -8480380.010
          })
            {name deleted}}`
        },
        path: "/cities/graphql"
      }, context, callback);
    });

    it('should create city unsuccessfully', (done) => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(400);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('City could not be created!');
        done();
      };
      handler.cities({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{createCity(newCity:
            {
              name:"name_create", 
              distribution:{
                fliersPerMission: 600,
                costPerMission: 0.16
              }, 
              purpose: {
                test: 15000,
                validate: 20000,
                growth: 25000,
                scaleUp: 30000
              },
              latitude: -1099202.010,
              longitude: -8480380.010
          })
            {id name deleted}}`
        },
        path: "/cities/graphql"
      }, context, callback);
    });
  });

});