
const expect = require('chai').expect;
const updateParams = require('../../api/cities/repository/update-city-params').default;

const city = {
    id: "108202",
    name: "city_name",
    distribution: {
        fliersPerMission: 600,
        costPerMission: 0.16
    },
    purpose: {
        test: 15000,
        validate: 20000,
        growth: 25000,
        scaleUp: 30000
    },
    latitude: -1099202.010,
    longitude: -8480380.010,
    deleted: false,
    createdAt: 0,
};

describe('update-city-params function', () => {
    let params;

    beforeEach(() => {
        delete process.env.TABLE_CITY;
        process.env.TABLE_CITY = 'dev-table-city';
        params = {
            TableName: process.env.TABLE_CITY
        }
    });

    it('should get updateCityParams successfully', async () => {
        const result = await updateParams(city, params);
        expect(`{"TableName":"dev-table-city","Key":{"id":"108202"},"ExpressionAttributeNames":{"#name":"name","#distribution":"distribution","#purpose":"purpose","#latitude":"latitude","#longitude":"longitude"},"ExpressionAttributeValues":{":name":"city_name",":distribution":{"fliersPerMission":600,"costPerMission":0.16},":purpose":{"test":15000,"validate":20000,"growth":25000,"scaleUp":30000},":latitude":-1099202.01,":longitude":-8480380.01},"UpdateExpression":"SET #name = :name  , #distribution = :distribution , #purpose = :purpose , #latitude = :latitude , #longitude = :longitude","ReturnValues":"ALL_NEW"}`)
            .to.be.equal(JSON.stringify(result))
    });
});