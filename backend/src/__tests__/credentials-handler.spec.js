

const expect = require('chai').expect;
const assert = require("chai").assert;

const credentialsHandler = require('../utils/credentials-handler');
const MockedDataStore = require('../__mock__/MockedDataStore').MockedDataStore;

const password = "password";
const passwordEncrypted = "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi";

const AUDIENCES = [
  {
    id: "108202",
    username: "username",
    email: "email@gmail.com",
    deleted: false,
    createdAt: 0,
    password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
  },
  {
    id: "0190383",
    username: "username2",
    email: "email2@gmail.com",
    deleted: false,
    createdAt: 1,
    password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
  }
];

const dataStore = new MockedDataStore(AUDIENCES);

describe('credentials-handler functions', () => {

  it('should compare a valid encryption', async () => {
    await credentialsHandler.bcryptCompare(password, passwordEncrypted)
      .then(success => {
        expect(success).to.be.equal(true);
      }).catch(error => {
        expect(error).to.be.null;
      })
  });

  it('should compare an invalid encryption', async () => {
    await credentialsHandler.bcryptCompare(password + '1', passwordEncrypted)
      .then(success => {
        expect(success).to.be.equal(false);
      }).catch(error => {
        expect(error.toString()).to.be.equal('Error: Incorrect password!');
      })
  });

  it('should hash password successfully', async () => {
    await credentialsHandler.hashPassword(password)
      .then(hash => {
        expect(hash).to.be.not.null;
      }).catch(error => {
        expect(error).to.be.null;
      })
  });

  it('should verify default credentials successfully', async () => {
    await credentialsHandler.verifyDefaultCredentials(dataStore, password, 'username')
      .then(resp => {
        expect(resp).to.be.not.null;
        expect(resp).to.be.equal(AUDIENCES[0]);
      }).catch(error => {
        expect(error).to.be.null;
      })
  });

  it('should verify default credentials incorrect username', async () => {
    await credentialsHandler.verifyDefaultCredentials(dataStore, password, 'username-invalid')
      .then(resp => {
        expect(resp).to.be.null;
      }).catch(error => {
        expect(error.toString()).to.be.equal('Error: Incorrect username!');
      })
  });

  it('should verify default credentials incorrect password', async () => {
    await credentialsHandler.verifyDefaultCredentials(dataStore, password + "3", 'username')
      .then(resp => {
        expect(resp).to.be.null;
      }).catch(error => {
        expect(error.toString()).to.be.equal('Error: Incorrect password!');
      })
  });

  it('should verify if user not exist successfully', async () => {
    const result = await credentialsHandler.verifyIfUserNotExist({
      request: {
        body: {
          email: "email@gmail.com3",
          username: ""
        }
      }
    }, dataStore)
      .then(r => expect(r).to.be.true)
      .catch(e => assert.fail());
  });

  it('should verify if user not exist unsuccessfully', async () => {
    const result = await credentialsHandler.verifyIfUserNotExist({
      request: {
        body: {
          email: "email@gmail.com",
          username: ""
        }
      }
    }, dataStore)
      .then(r => assert.fail())
      .catch(e => expect(e.errorMessage).to.be.equal('{"code":401,"message":"User already exists!"}'));
  });

});