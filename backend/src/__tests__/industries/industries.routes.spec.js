

const expect = require('chai').expect;
const MockedDataBase = require('../../__mock__/MockedDataBase').MockedDataBase;

const __id = 'aujiaikoak9903903-3093';
const _username = 'email';

const user = {
  _id: __id,
  username: _username,
  roles: "admin"
};

const INDUSTRIES = [
  {
    id: "108202",
    name: "name",
    cvr: 0.15,
    deleted: false,
    createdAt: 0,
  },
  {
    id: "0190383",
    name: "name2",
    cvr: 0.15,
    deleted: false,
    createdAt: 1,
  }
];

describe('handler industries api', () => {
  let handler;
  let token;
  let routes;
  let tokenHandler;

  beforeEach(() => {
    delete process.env.JWT_SECRET_KEY;
    delete process.env.TABLE_INDUSTRY;
    process.env.JWT_SECRET_KEY = 'JWT_SECRET_KEY-TEST';
    process.env.TABLE_INDUSTRY = 'dev-table-industry';

    routes = require('../../api/industries/routers');

    let industries = JSON.parse(JSON.stringify(INDUSTRIES));
    mockedDataBase = new MockedDataBase(industries);
    routes.setupTest(mockedDataBase);

    handler = require('../../../handler');
    tokenHandler = require('../../utils/token-handler');
    token = tokenHandler.createToken(user);
  });

  describe('getAll', () => {
    it('should fetch industries list successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"industries":{"items":[{"id":"108202","name":"name","cvr":0.15},{"id":"0190383","name":"name2","cvr":0.15}]}}}')
          .to.be.equal(response.body);
      };
      await handler.industries({
        headers: {
          authorization: `Bearer ${token}`
        },
        httpMethod: "GET",
        body: "",
        path: "/industries/graphql",
        "queryStringParameters": {
          "query": "{industries(page:1, limit:10){items{id name cvr}}}"
        }
      }, context, callback);
    });

    it('should fetch industries list unsuccessfully', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect('Could not fetch industry\'s list!')
          .to.be.equal(JSON.parse(response.body).errors[0].message);
      };
      await handler.industries({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/industries/graphql",
        "queryStringParameters": {
          "query": "{industries(page:1, limit:10){items{id name cvr}}}"
        }
      }, context, callback);
    });
  });

  describe('get', () => {
    it('should find industry by id successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"industry":{"id":"108202","name":"name","cvr":0.15}}}')
          .to.be.equal(response.body);
      };
      await handler.industries({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/industries/graphql",
        "queryStringParameters": {
          "query": '{industry(id:"108202"){id name cvr}}'
        }
      }, context, callback);
    });

    it('should find industry by id unsuccessfully', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('Industry not found!');
      };
      await handler.industries({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/industries/graphql",
        "queryStringParameters": {
          "query": '{industry(id:"108202-Invalid"){id name cvr}}'
        }
      }, context, callback);
    });
  });

  describe('delete', () => {

    it('should delete industry successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"deleteIndustry":{"id":"108202","name":"name","cvr":0.15,"deleted":true}}}')
          .to.be.equal(response.body);
      };
      await handler.industries({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": 'mutation{deleteIndustry(id:"108202"){id name cvr deleted}}'
        },
        path: "/industries/graphql"
      }, context, callback);
    });

    it('should not delete industry not find', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect('Industry not found!')
          .to.be.equal(JSON.parse(response.body).errors[0].message);
      };
      await handler.industries({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": 'mutation{deleteIndustry(id:"108202-Invalid"){id name cvr deleted}}'
        },
        path: "/industries/graphql"
      }, context, callback);
    });

  });

  describe('update', () => {

    it('should update industry successfully', (done) => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"updateIndustry":{"id":"108202","name":"name_update","cvr":0.16}}}')
          .to.be.equal(response.body);
        done();
      };
      handler.industries({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{updateIndustry(industry:{id:"108202", name:"name_update", cvr: 0.16})
            {id name cvr}}`
        },
        path: "/industries/graphql"
      }, context, callback);
    });

    it('should update industry unsuccessfully', (done) => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = async (error, response) => {
        expect(response.statusCode).to.be.equal(400);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('Industry could not be updated!');
        done();
      };
      handler.industries({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{updateIndustry(industry:{id:"108202", name:"name_update"})
            {id name cvr}}`
        },
        path: "/industries/graphql"
      }, context, callback);
    });

  });

  describe('create', () => {
    it('should create industry successfully', (done) => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"createIndustry":{"name":"name_create","cvr":0.36,"deleted":false}}}')
          .to.be.equal(response.body);
        done();
      };
      handler.industries({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{createIndustry(newIndustry:
            {name:"name_create", cvr:0.36
          })
            {name cvr deleted}}`
        },
        path: "/industries/graphql"
      }, context, callback);
    });

    it('should create industry unsuccessfully', (done) => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(400);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('Industry could not be created!');
        done();
      };
      handler.industries({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{createIndustry(newIndustry:
            {name:"name_create", cvr:0.36
          })
            {name cvr deleted}}`
        },
        path: "/industries/graphql"
      }, context, callback);
    });
  });

});