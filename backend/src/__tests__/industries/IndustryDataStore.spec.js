const expect = require('chai').expect;
const MockedDataBase = require('../../__mock__/MockedDataBase').MockedDataBase;

const INDUSTRIES = [
    {
      id: "108202",
      name: "name",
      cvr: 0.15,
      deleted: false,
      createdAt: 0,
    },
    {
      id: "0190383",
      name: "name2",
      cvr: 0.15,
      deleted: false,
      createdAt: 1,
    }
  ];

describe('IndustryDataStore function', () => {
    let IndustryDataStore;
    let dataStore;

    beforeEach(() => {
        IndustryDataStore = require('../../api/industries/repository/IndustryDataStore').IndustryDataStore;
        mockedDataBase = new MockedDataBase(INDUSTRIES)
        mockedDataBase.setupError(false);
        dataStore = new IndustryDataStore(mockedDataBase);
    });

    describe('getAll', () => {
        it('should get all industries successfully', async () => {
            const result = await dataStore.getAll(1, 3);
            expect(result.data.items.length).to.be.equal(2);
        });

        it('should get all industries unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.getAll(1, 3);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('get', () => {
        it('should get industry successfully', async () => {
            const result = await dataStore.get('id', INDUSTRIES[0].id);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(INDUSTRIES[0].id);
        });

        it('should get industry unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.get('id', 'invalid-id');
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('create', () => {
        it('should create industry successfully', async () => {
            const result = await dataStore.create(INDUSTRIES[0], false);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(INDUSTRIES[0].id);
        });

        it('should create industry unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.create(INDUSTRIES[0], false);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('update', () => {
        it('should update industry successfully', async () => {
            const result = await dataStore.update(INDUSTRIES[0]);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(INDUSTRIES[0].id);
        });

        it('should update industry unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.update(INDUSTRIES[0]);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('delete', () => {
        it('should delete industry successfully', async () => {
            const result = await dataStore.remove(INDUSTRIES[0]);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(INDUSTRIES[0].id);
        });

        it('should delete industry unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.remove(INDUSTRIES[0]);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

});