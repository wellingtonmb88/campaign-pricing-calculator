
const expect = require('chai').expect;
const updateParams = require('../../api/industries/repository/update-industry-params').default;

const INDUSTRY = {
    id: "108202",
    name: "name",
    cvr: 0.15,
    deleted: false,
    createdAt: 0,
};

describe('update-industry-params function', () => {
    let params;

    beforeEach(() => {
        delete process.env.TABLE_INDUSTRY;
        process.env.TABLE_INDUSTRY = 'dev-table-industry';
        params = {
            TableName: process.env.TABLE_INDUSTRY
        }
    });

    it('should get updateIndustryParams successfully', async () => {
        const result = await updateParams(INDUSTRY, params);
        expect(`{"TableName":"dev-table-industry","Key":{"id":"108202"},"ExpressionAttributeNames":{"#name":"name","#cvr":"cvr"},"ExpressionAttributeValues":{":name":"name",":cvr":0.15},"UpdateExpression":"SET #name = :name  , #cvr = :cvr","ReturnValues":"ALL_NEW"}`)
            .to.be.equal(JSON.stringify(result))
    });
});