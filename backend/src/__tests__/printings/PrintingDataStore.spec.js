const expect = require('chai').expect;
const MockedDataBase = require('../../__mock__/MockedDataBase').MockedDataBase;

const PRINTINGS = [
    {
      id: "108202",
      orders: 6000,
      rate: 0.15,
      deleted: false,
      createdAt: 0,
    },
    {
      id: "0190383",
      orders: 7000,
      rate: 0.16,
      deleted: false,
      createdAt: 1,
    }
  ];
  

describe('PrintingDataStore function', () => {
    let PrintingDataStore;
    let dataStore;

    beforeEach(() => {
        PrintingDataStore = require('../../api/printings/repository/PrintingDataStore').PrintingDataStore;
        mockedDataBase = new MockedDataBase(PRINTINGS)
        mockedDataBase.setupError(false);
        dataStore = new PrintingDataStore(mockedDataBase);
    });

    describe('getAll', () => {
        it('should get all printings successfully', async () => {
            const result = await dataStore.getAll(1, 3);
            expect(result.data.items.length).to.be.equal(2);
        });

        it('should get all printings unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.getAll(1, 3);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('get', () => {
        it('should get printing successfully', async () => {
            const result = await dataStore.get('id', PRINTINGS[0].id);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(PRINTINGS[0].id);
        });

        it('should get printing unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.get('id', 'invalid-id');
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('create', () => {
        it('should create printing successfully', async () => {
            const result = await dataStore.create(PRINTINGS[0], false);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(PRINTINGS[0].id);
        });

        it('should create printing unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.create(PRINTINGS[0], false);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('update', () => {
        it('should update printing successfully', async () => {
            const result = await dataStore.update(PRINTINGS[0]);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(PRINTINGS[0].id);
        });

        it('should update printing unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.update(PRINTINGS[0]);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('delete', () => {
        it('should delete printing successfully', async () => {
            const result = await dataStore.remove(PRINTINGS[0]);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(PRINTINGS[0].id);
        });

        it('should delete printing unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.remove(PRINTINGS[0]);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

});