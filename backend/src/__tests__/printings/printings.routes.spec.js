

const expect = require('chai').expect;
const MockedDataBase = require('../../__mock__/MockedDataBase').MockedDataBase;

const __id = 'aujiaikoak9903903-3093';
const _username = 'email';

const user = {
  _id: __id,
  username: _username,
  roles: "admin"
};

const PRINTINGS = [
  {
    id: "108202",
    orders: 6000,
    rate: 0.15,
    deleted: false,
    createdAt: 0,
  },
  {
    id: "0190383",
    orders: 7000,
    rate: 0.16,
    deleted: false,
    createdAt: 1,
  }
];

describe('handler printings api', () => {
  let handler;
  let token;
  let routes;
  let tokenHandler;

  beforeEach(() => {
    delete process.env.JWT_SECRET_KEY;
    delete process.env.TABLE_PRINTING;
    process.env.JWT_SECRET_KEY = 'JWT_SECRET_KEY-TEST';
    process.env.TABLE_PRINTING = 'dev-table-printing';

    routes = require('../../api/printings/routers');

    let printings = JSON.parse(JSON.stringify(PRINTINGS));
    mockedDataBase = new MockedDataBase(printings);
    routes.setupTest(mockedDataBase);

    handler = require('../../../handler');
    tokenHandler = require('../../utils/token-handler');
    token = tokenHandler.createToken(user);
  });

  describe('getAll', () => {
    it('should fetch printings list successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"printings":{"items":[{"id":"108202","orders":6000,"rate":0.15},{"id":"0190383","orders":7000,"rate":0.16}]}}}')
          .to.be.equal(response.body);
      };
      await handler.printings({
        headers: {
          authorization: `Bearer ${token}`
        },
        httpMethod: "GET",
        body: "",
        path: "/printings/graphql",
        "queryStringParameters": {
          "query": "{printings(page:1, limit:10){items{id orders rate}}}"
        }
      }, context, callback);
    });

    it('should fetch printings list unsuccessfully', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect('Could not fetch printing\'s list!')
          .to.be.equal(JSON.parse(response.body).errors[0].message);
      };
      await handler.printings({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/printings/graphql",
        "queryStringParameters": {
          "query": "{printings(page:1, limit:10){items{id orders rate}}}"
        }
      }, context, callback);
    });
  });

  describe('get', () => {
    it('should find printing by id successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"printing":{"id":"108202","orders":6000,"rate":0.15}}}')
          .to.be.equal(response.body);
      };
      await handler.printings({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/printings/graphql",
        "queryStringParameters": {
          "query": '{printing(id:"108202"){id orders rate}}'
        }
      }, context, callback);
    });

    it('should find printing by id unsuccessfully', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('Printing not found!');
      };
      await handler.printings({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/printings/graphql",
        "queryStringParameters": {
          "query": '{printing(id:"108202-Invalid"){id orders rate}}'
        }
      }, context, callback);
    });
  });

  describe('delete', () => {

    it('should delete printing successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"deletePrinting":{"id":"108202","orders":6000,"rate":0.15,"deleted":true}}}')
          .to.be.equal(response.body);
      };
      await handler.printings({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": 'mutation{deletePrinting(id:"108202"){id orders rate deleted}}'
        },
        path: "/printings/graphql"
      }, context, callback);
    });

    it('should not delete printing not find', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect('Printing not found!')
          .to.be.equal(JSON.parse(response.body).errors[0].message);
      };
      await handler.printings({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": 'mutation{deletePrinting(id:"108202-Invalid"){id orders rate deleted}}'
        },
        path: "/printings/graphql"
      }, context, callback);
    });

  });

  describe('update', () => {

    it('should update printing successfully', (done) => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"updatePrinting":{"id":"108202","orders":8000,"rate":0.3}}}')
          .to.be.equal(response.body);
        done();
      };
      handler.printings({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{updatePrinting(id:"108202", orders:8000, rate:0.30)
            {id orders rate}}`
        },
        path: "/printings/graphql"
      }, context, callback);
    });

    it('should update printing unsuccessfully', (done) => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = async (error, response) => {
        expect(response.statusCode).to.be.equal(400);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('Printing could not be updated!');
        done();
      };
      handler.printings({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{updatePrinting(id:"108202", orders:8000)
            {id orders rate}}`
        },
        path: "/printings/graphql"
      }, context, callback);
    });

  });

  describe('create', () => {
    it('should create printing successfully', (done) => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"createPrinting":{"orders":8000,"rate":0.36,"deleted":false}}}')
          .to.be.equal(response.body);
        done();
      };
      handler.printings({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{createPrinting(orders:8000, rate:0.36)
            {orders rate deleted}}`
        },
        path: "/printings/graphql"
      }, context, callback);
    });

    it('should create printing unsuccessfully', (done) => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(400);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('Printing could not be created!');
        done();
      };
      handler.printings({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{createPrinting(orders:8000, rate:0.36)
            {orders rate deleted}}`
        },
        path: "/printings/graphql"
      }, context, callback);
    });
  });

});