
const expect = require('chai').expect;
const updateParams = require('../../api/printings/repository/update-printing-params').default;

const PRINTING = {
    id: "108202",
    orders: 6000,
    rate: 0.15,
    deleted: false,
    createdAt: 0,
};

describe('update-printing-params function', () => {
    let params;

    beforeEach(() => {
        delete process.env.TABLE_PRINTING;
        process.env.TABLE_PRINTING = 'dev-table-printing';
        params = {
            TableName: process.env.TABLE_PRINTING
        }
    });

    it('should get updatePrintingParams successfully', async () => {
        const result = await updateParams(PRINTING, params);
        expect(`{"TableName":"dev-table-printing","Key":{"id":"108202"},"ExpressionAttributeNames":{"#orders":"orders","#rate":"rate"},"ExpressionAttributeValues":{":orders":6000,":rate":0.15},"UpdateExpression":"SET #orders = :orders  , #rate = :rate","ReturnValues":"ALL_NEW"}`)
            .to.be.equal(JSON.stringify(result))
    });
});