

const expect = require('chai').expect;
const MockedDataBase = require('../../__mock__/MockedDataBase').MockedDataBase;

const __id = 'aujiaikoak9903903-3093';
const _username = 'email';

const user = {
  _id: __id,
  username: _username,
  roles: "admin"
};

const AUDIENCES = [
  {
    id: "108202",
    username: "username",
    email: "email@gmail.com",
    deleted: false,
    createdAt: 0,
    password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
  },
  {
    id: "0190383",
    username: "username2",
    email: "email2@gmail.com",
    deleted: false,
    createdAt: 1,
    password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
  }
];

describe('handler audiences api', () => {
  let handler;
  let token;
  let routes;
  let tokenHandler;

  beforeEach(() => {
    delete process.env.JWT_SECRET_KEY;
    delete process.env.TABLE_AUDIENCE;
    process.env.JWT_SECRET_KEY = 'JWT_SECRET_KEY-TEST';
    process.env.TABLE_AUDIENCE = 'dev-table-audience';

    routes = require('../../api/audiences/routers');

    let audiences = JSON.parse(JSON.stringify(AUDIENCES));
    mockedDataBase = new MockedDataBase(audiences);
    routes.setupTest(mockedDataBase);

    handler = require('../../../handler');
    tokenHandler = require('../../utils/token-handler');
    token = tokenHandler.createToken(user);
  });

  describe('getAll', () => {
    it('should fetch audiences list successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"audiences":{"items":[{"id":"108202","name":null,"username":"username","email":"email@gmail.com"},{"id":"0190383","name":null,"username":"username2","email":"email2@gmail.com"}]}}}')
          .to.be.equal(response.body);
      };
      await handler.audiences({
        headers: {
          authorization: `Bearer ${token}`
        },
        httpMethod: "GET",
        body: "",
        path: "/audiences/graphql",
        "queryStringParameters": {
          "query": "{audiences(page:1, limit:10){items{id name username email}}}"
        }
      }, context, callback);
    });

    it('should fetch audiences list unsuccessfully', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect('Could not fetch audience\'s list!')
          .to.be.equal(JSON.parse(response.body).errors[0].message);
      };
      await handler.audiences({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/audiences/graphql",
        "queryStringParameters": {
          "query": "{audiences(page:1, limit:10){items{id name username email}}}"
        }
      }, context, callback);
    });
  });

  describe('get', () => {
    it('should find audience by id successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"audience":{"id":"108202","name":null,"username":"username","email":"email@gmail.com"}}}')
          .to.be.equal(response.body);
      };
      await handler.audiences({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/audiences/graphql",
        "queryStringParameters": {
          "query": '{audience(id:"108202"){id name username email}}'
        }
      }, context, callback);
    });

    it('should find audience by id unsuccessfully', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('Audience not found!');
      };
      await handler.audiences({
        headers: {
          authorization: `Bearer ${token}`
        },
        method: "GET",
        body: "",
        path: "/audiences/graphql",
        "queryStringParameters": {
          "query": '{audience(id:"108202-Invalid"){id name username email}}'
        }
      }, context, callback);
    });
  });

  describe('delete', () => {

    it('should delete audience successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"deleteAudience":{"id":"108202","name":null,"username":"username","email":"email@gmail.com"}}}')
          .to.be.equal(response.body);
      };
      await handler.audiences({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": 'mutation{deleteAudience(id:"108202"){id name username email}}'
        },
        path: "/audiences/graphql"
      }, context, callback);
    });

    it('should not delete audience not find', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(404);
        expect('Audience not found!')
          .to.be.equal(JSON.parse(response.body).errors[0].message);
      };
      await handler.audiences({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": 'mutation{deleteAudience(id:"108202-Invalid"){id name username email deleted}}'
        },
        path: "/audiences/graphql"
      }, context, callback);
    });

  });

  describe('update', () => {

    it('should update audience successfully', (done) => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('{"data":{"updateAudience":{"id":"108202","name":"name_update"}}}')
          .to.be.equal(response.body);
        done();
      };
      handler.audiences({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{updateAudience(audience:{id:"108202", name:"name_update"})
            {id name}}`
        },
        path: "/audiences/graphql"
      }, context, callback);
    });

    it('should update audience unsuccessfully', (done) => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = async (error, response) => {
        expect(response.statusCode).to.be.equal(400);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('Audience could not be updated!');
        done();
      };
      handler.audiences({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{updateAudience(audience:
            {id:"108202", name:"name_update", username:"username_update"})
            {id name username email deleted}}`
        },
        path: "/audiences/graphql"
      }, context, callback);
    });

  });

  describe('create', () => {
    it('should create audience successfully', (done) => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        expect('email_create@gmail.com')
          .to.be.equal(JSON.parse(response.body).data.createAudience['email']);
        done();
      };
      handler.audiences({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{createAudience(newAudience:
            {name:"name_create", username:"username_create", email:"email_create@gmail.com",
            password:"password", role:"admin"
          })
            {id name username email role deleted}}`
        },
        path: "/audiences/graphql"
      }, context, callback);
    });

    it('should create audience unsuccessfully', (done) => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(400);
        expect(JSON.parse(response.body).errors[0].message)
          .to.be.equal('Audience could not be created!');
        done();
      };
      handler.audiences({
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`
        },
        httpMethod: "POST",
        body: {
          "query": `mutation{createAudience(newAudience:
            {name:"name_create", username:"username_create", email:"email_create",
            password:"password", role:"admin"
          })
            {id name username email role deleted}}`
        },
        path: "/audiences/graphql"
      }, context, callback);
    });
  });

});