
const expect = require('chai').expect;
const updateParams = require('../../api/audiences/repository/update-audience-params').default;

const audience = {
    id: 'audienceId',
    name: 'name',
    username: 'username',
    email: 'email@gmail.com',
    deleted: false,
    password: 'password', 
    role: 'admin'
};

describe('update-audience-params function', () => {
    let params;

    beforeEach(() => {
        delete process.env.TABLE_AUDIENCE;
        process.env.TABLE_AUDIENCE = 'dev-table-audience';
        params = {
            TableName: process.env.TABLE_AUDIENCE
        }
    });

    it('should get updateAudienceParams successfully', async () => {
        const result = await updateParams(audience, params);
        expect(`{"TableName":"dev-table-audience","Key":{"id":"audienceId"},"ExpressionAttributeNames":{"#name":"name","#username":"username","#email":"email","#password":"password"},"ExpressionAttributeValues":{":name":"name",":username":"username",":email":"email@gmail.com",":password":"password"},"UpdateExpression":"SET #name = :name  , #username = :username , #email = :email , #password = :password","ReturnValues":"ALL_NEW"}`)
            .to.be.equal(JSON.stringify(result))
    });
});