const expect = require('chai').expect;
const MockedDataBase = require('../../__mock__/MockedDataBase').MockedDataBase;

const AUDIENCES = [
    {
        id: "108202",
        username: "username",
        email: "email@gmail.com",
        deleted: false,
        createdAt: 0,
        password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
    },
    {
        id: "0190383",
        username: "username2",
        email: "email2@gmail.com",
        deleted: false,
        createdAt: 1,
        password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
    }
];

describe('AudienceDataStore function', () => {
    let AudienceDataStore;
    let dataStore;

    beforeEach(() => {
        AudienceDataStore = require('../../api/audiences/repository/AudienceDataStore').AudienceDataStore;
        mockedDataBase = new MockedDataBase(AUDIENCES)
        mockedDataBase.setupError(false);
        dataStore = new AudienceDataStore(mockedDataBase);
    });

    describe('getAll', () => {
        it('should get all audiences successfully', async () => {
            const result = await dataStore.getAll(1, 3);
            expect(result.data.items.length).to.be.equal(2);
        });

        it('should get all audiences unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.getAll(1, 3);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('get', () => {
        it('should get audience successfully', async () => {
            const result = await dataStore.get('id', AUDIENCES[0].id);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(AUDIENCES[0].id);
        });

        it('should get audience unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.get('id', 'invalid-id');
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('create', () => {
        it('should create audience successfully', async () => {
            const result = await dataStore.create(AUDIENCES[0], false);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(AUDIENCES[0].id);
        });

        it('should create audience unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.create(AUDIENCES[0], false);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('update', () => {
        it('should update audience successfully', async () => {
            const result = await dataStore.update(AUDIENCES[0]);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(AUDIENCES[0].id);
        });

        it('should update audience unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.update(AUDIENCES[0]);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

    describe('delete', () => {
        it('should delete audience successfully', async () => {
            const result = await dataStore.remove(AUDIENCES[0]);
            expect(result.data).to.be.not.null;
            expect(result.data.id).to.be.equal(AUDIENCES[0].id);
        });

        it('should delete audience unsuccessfully', async () => {
            mockedDataBase.setupError(true);
            const result = await dataStore.remove(AUDIENCES[0]);
            expect(result.error).to.be.equal(true);
            expect(result.data).to.be.null;
        });
    });

});