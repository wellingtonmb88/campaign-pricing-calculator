

const expect = require("chai").expect;
const assert = require("chai").assert;
const dynamoDb = require("serverless-dynamodb-client").doc;

const DaseModel = require("../model/BaseModel").BaseModel;
const dynamoDbManager = require("../repository/DynamoDbManager");
const AWS = require("aws-sdk");

const databaseManager = new dynamoDbManager.DynamoDbManager();
const item = new DaseModel();
item.id = "id";
item.email = "email@gmail.com";
item.createdAt = "123456";
item.deleted = false;

describe("DynamoDbManager functions", () => {
  let params;

  afterEach(() => {
    dynamoDbManager.initializateDynamoClient(dynamoDb);
  });

  beforeEach(() => {
    delete process.env.TABLE_MEMBER;
    process.env.TABLE_PRINTING = "dev-table-printing";

    const dynamo = new AWS.DynamoDB.DocumentClient({
      region: "localhost",
      endpoint: "http://localhost:8000"
    });

    dynamoDbManager.initializateDynamoClient(dynamo);
    params = {
      TableName: process.env.TABLE_PRINTING
    }
  });

  describe("save", () => {
    it("should save item successfully", async () => {
      const result = await databaseManager.save(item, params);
      expect(result.error).to.be.equal(false);
      expect(result.success).to.be.equal(true);
    });

    it("should not save", async () => {
      const resultSave = await databaseManager.save(item, {});
      expect(resultSave.error).to.be.equal(true);
      expect(resultSave.success).to.be.equal(false);
    });

    it("should not save with undefined params", async () => {
      await databaseManager.save(item, undefined)
        .catch(error => { expect(error.toString()).to.be.equal("Error: params property is required."); });
    });
  });

  describe("get", () => {
    it("should get item by email successfully", async () => {
      const resultGet = await databaseManager.get("email", item.email, params);
      expect(resultGet.data.email).to.be.equal(item.email);
    });

    it("should not find item by email", async () => {
      const resultGet = await databaseManager.get("email", "another-email", params);
      expect(resultGet.error).to.be.equal(false);
      expect(resultGet.data).to.be.empty;
    });

    it("should get item by email unsuccessfully", async () => {
      const resultGet = await databaseManager.get("email", item.email, {});
      expect(resultGet.error).to.be.equal(true);
      expect(resultGet.data).to.be.null;
    });

    it("should get item by id successfully", async () => {
      const resultGet = await databaseManager.get("id", item.id, params);
      expect(resultGet.data.id).to.be.equal(item.id);
    });

    it("should not find item by id", async () => {
      const resultGet = await databaseManager.get("id", "another-id", params);
      expect(resultGet.error).to.be.equal(false);
      expect(resultGet.data).to.be.empty;
    });

    it("should get item by id unsuccessfully", async () => {
      const resultGet = await databaseManager.get("id", item.id, {});
      expect(resultGet.error).to.be.equal(true);
      expect(resultGet.data).to.be.null;
    });

  });

  describe("getAll", () => {
    it("should get all items successfully", async () => {
      item.id = "id2";
      await databaseManager.save(item, params);
      item.id = "id3";
      await databaseManager.save(item, params);
      item.id = "id4";
      await databaseManager.save(item, params);
      const resultGetAll = await databaseManager.getAll(1, 3, params);
      expect(resultGetAll.data.items.length).to.be.equal(3);
    });

    it("should get all items unsuccessfully", async () => {
      const resultGetAll = await databaseManager.getAll(1, 3, {});
      expect(resultGetAll.error).to.be.equal(true);
      expect(resultGetAll.data).to.be.null;
    });

    it("should not get all items with undefined params", async () => {
      await databaseManager.getAll(1, 3, undefined)
        .catch(error => { expect(error.toString()).to.be.equal("Error: params property is required."); });
    });
  });

  describe("update", () => {
    it("should update item successfully", async () => {
      const newParams = {
        TableName: params.TableName,
        Key: { id: item.id },
        ReturnValues: "ALL_NEW"
      };
      const result = await databaseManager.update(item, newParams);
      expect(result.error).to.be.equal(false);
      expect(result.success).to.be.equal(true);
    });

    it("should update item unsuccessfully", async () => {
      const result = await databaseManager.update(item, {});
      expect(result.error).to.be.equal(true);
      expect(result.success).to.be.equal(false);
    });

    it("should not update item with undefined params", async () => {
      await databaseManager.update(item, undefined)
        .catch(error => { expect(error.toString()).to.be.equal("Error: params property is required."); });
    });
  });

  describe("delete", () => {
    it("should delete item successfully", async () => {
      const result = await databaseManager.delete(item, params);
      expect(result.error).to.be.equal(false);
      expect(result.success).to.be.equal(true);
    });

    it("should delete item unsuccessfully", async () => {
      const result = await databaseManager.delete(item, {});
      expect(result.error).to.be.equal(true);
      expect(result.success).to.be.equal(false);
    });

    it("should not delete item with undefined params", async () => {
      await databaseManager.delete(item, undefined)
        .catch(error => { expect(error.toString()).to.be.equal("Error: params property is required."); });
    });

  });
});