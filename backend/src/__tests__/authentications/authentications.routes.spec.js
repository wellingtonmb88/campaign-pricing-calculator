

const expect = require('chai').expect;
const MockedDataBase = require('../../__mock__/MockedDataBase').MockedDataBase;

const __id = 'aujiaikoak9903903-3093';
const _username = 'email';

const user = {
  _id: __id,
  username: _username,
  roles: "admin"
};

const AUDIENCES = [
  {
    id: "108202",
    username: "username",
    email: "email@gmail.com",
    role: "admin",
    deleted: false,
    createdAt: 0,
    password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
  },
  {
    id: "0190383",
    username: "username2",
    email: "email2@gmail.com",
    role: "support",
    deleted: false,
    createdAt: 1,
    password: "$2a$10$RPJOWE/m9JVblyOoLzGpN.r1h.jyVFi2kesVH2Dn4DtERvh87A0Qi" //password
  }
];

describe('handler authentications api', () => {
  let handler;
  let token;
  let routes;
  let tokenHandler;

  beforeEach(() => {
    delete process.env.JWT_SECRET_KEY;
    delete process.env.TABLE_AUDIENCE;
    process.env.JWT_SECRET_KEY = 'JWT_SECRET_KEY-TEST';
    process.env.TABLE_AUDIENCE = 'dev-table-audience';

    routes = require('../../api/authentications/routers');

    let audiences = JSON.parse(JSON.stringify(AUDIENCES));
    mockedDataBase = new MockedDataBase(audiences);
    routes.setupTest(mockedDataBase);

    handler = require('../../../handler');
    tokenHandler = require('../../utils/token-handler');
    token = tokenHandler.createToken(user);
  });

  describe('authenticate', () => {
    it('authenticate user successfully', async () => {
      mockedDataBase.setupError(false);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(200);
        const token = JSON.parse(response.body).data.authenticate.token;
        expect(token).to.be.not.null;
      };
      await handler.authentications({
        httpMethod: "GET",
        body: "",
        path: "/authentications/graphql",
        "queryStringParameters": {
          "query": '{authenticate(username:"username", password: "password"){token}}'
        }
      }, context, callback);
    });

    it('should not authenticate user with invalid username', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(401);
        const token = JSON.parse(response.body).data.authenticate;
        expect(token).to.be.null;
        const _error = JSON.parse(response.body).errors[0];
        expect(_error.message).to.be.equal('{"statusCode":401,"error":"Unauthorized","message":"Incorrect username!"}');
      };
      await handler.authentications({
        httpMethod: "GET",
        body: "",
        path: "/authentications/graphql",
        "queryStringParameters": {
          "query": '{authenticate(username:"username-invalid", password: "password"){token}}'
        }
      }, context, callback);
    });

    it('should not authenticate user with invalid password', async () => {
      mockedDataBase.setupError(true);
      expect(token).not.to.be.null;
      const event = 'event';
      const context = {};
      const callback = (error, response) => {
        expect(response.statusCode).to.be.equal(401);
        const token = JSON.parse(response.body).data.authenticate;
        expect(token).to.be.null;
        const _error = JSON.parse(response.body).errors[0];
        expect(_error.message).to.be.equal('{"statusCode":401,"error":"Unauthorized","message":"Incorrect password!"}');
      };
      await handler.authentications({
        httpMethod: "GET",
        body: "",
        path: "/authentications/graphql",
        "queryStringParameters": {
          "query": '{authenticate(username:"username", password: "password-invalid"){token}}'
        }
      }, context, callback);
    });
    
  });
});