
import * as Koa from "koa";
import * as koaBody from "koa-body";
import * as errorHandler from "../../utils/error-handler";
import * as authorizer from "../../middleware/authorizer";
import { responseHandler } from "../../middleware/response-handler";
import { DynamoDbManager } from "../../repository/DynamoDbManager";
import { AudienceDataStore } from "../audiences/repository/AudienceDataStore";
import { graphqlKoa, graphiqlKoa } from "apollo-server-koa";
import { makeExecutableSchema } from "graphql-tools";

const serverless = require("serverless-http");
const Router = require("koa-router");
const router = new Router();
const app = new Koa();

const dataBaseManager = new DynamoDbManager();
let dataStore = new AudienceDataStore(dataBaseManager);

export const setupTest = (mockDataBaseManager) => {
    dataStore = new AudienceDataStore(mockDataBaseManager);
};

// The GraphQL schema in string form
const typeDefs = `
    type Token {
        token: String!
    }
    """
    Query Authentications
    """
    type Query {
        """
        Authenticate an User
        """
        authenticate(username: String!, password: String!): Token
    }
`;

const resolvers = {
    Query: {
        async authenticate(_, { username, password }, ctx) {
            const result = await authorizer
                .authorizeDefaultCredentials(dataStore, password, username);
            if (result.status !== 202) {
                errorHandler.sendError(ctx, result.body, result.status);
                return;
            }
            return {
                token: result.body,
            };
        },
    },
};

const schemas = makeExecutableSchema({
    resolvers,
    typeDefs,
});

const ENDPOINT = "/authentications/graphql";

router.post(ENDPOINT,
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

router.get(ENDPOINT,
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

if (process.env.ENABLE_GRAPHIQL) {
    router.get("/authentications/graphiql", graphiqlKoa({ endpointURL: ENDPOINT }));
}

app.use(koaBody())
    .use(responseHandler)
    .use(router.routes())
    .use(router.allowedMethods());

export const authenticationsServer = serverless(app);
