
import * as Koa from "koa";
import * as koaBody from "koa-body";
import * as authorizer from "../../middleware/authorizer";
import * as printingUseCase from "./usecases/index";
import { responseHandler } from "../../middleware/response-handler";
import { DynamoDbManager } from "../../repository/DynamoDbManager";
import { PrintingDataStore } from "./repository/PrintingDataStore";
import { graphqlKoa, graphiqlKoa } from "apollo-server-koa";
import { makeExecutableSchema } from "graphql-tools";

const serverless = require("serverless-http");
const Router = require("koa-router");
const router = new Router();
const app = new Koa();

const dataBaseManager = new DynamoDbManager();
let dataStore = new PrintingDataStore(dataBaseManager);

export const setupTest = (mockPrintingDataBaseManager) => {
    dataStore = new PrintingDataStore(mockPrintingDataBaseManager);
};

// The GraphQL schema in string form
const typeDefs = `
    """
    Model represanting a Printing object
    """
    type Printing {
        id          : String
        orders      : Int
        rate        : Float
        deleted     : Boolean
        createdAt   : Float
    }
    """
    Object to format the response of pagination query
    """
    type Items {
        items: [Printing]
        count: Int
        page: Int
    }
    """
    Querying Printing
    """
    type Query {
        """
        Search Printings with pagination
        """
        printings(page:  Int!, limit: Int): Items,
        """
        Search Printing by Id
        """
        printing(id: String!): Printing
    }
    """
    Mutating Printing
    """
    type Mutation {
        """
        Create a new Printing
        """
        createPrinting (orders: Int!, rate: Float!): Printing,
        """
        Update a Printing
        """
        updatePrinting (id: String!, orders: Int, rate: Float): Printing,
        """
        Delete a Printing
        """
        deletePrinting (id: String!): Printing
    }
`;

const resolvers = {
    Mutation: {
        async createPrinting(_, { orders, rate }, ctx) {
            ctx.request.body.orders = orders;
            ctx.request.body.rate = rate;
            await printingUseCase.create.create(ctx, dataStore);
            return ctx.body;
        },
        async updatePrinting(_, { id, orders, rate, deleted }, ctx) {
            ctx.request.body.id = id;
            ctx.request.body.orders = orders;
            ctx.request.body.rate = rate;
            await printingUseCase.update.default(ctx, dataStore);
            return ctx.body;
        },
        async deletePrinting(_, { id }, ctx) {
            ctx.params.id = id;
            await printingUseCase.remove(ctx, dataStore);
            return ctx.body;
        },
    },
    Query: {
        async printings(_, { page, limit }, ctx) {
            ctx.query.page = page;
            ctx.query.limit = limit;
            await printingUseCase.getAll(ctx, dataStore);
            return ctx.body;
        },
        async printing(_, { id }, ctx) {
            ctx.id = id;
            await printingUseCase.get(ctx, dataStore);
            return ctx.body;
        },
    },
};

const schemas = makeExecutableSchema({
    resolvers,
    typeDefs,
});

const ENDPOINT = "/printings/graphql";

router.post(ENDPOINT,
    authorizer.authScope({ admin: true, support: true, common: false }),
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

router.get(ENDPOINT,
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

if (process.env.ENABLE_GRAPHIQL) {
    router.get("/printings/graphiql", graphiqlKoa({ endpointURL: ENDPOINT }));
}

app.use(koaBody())
    .use(responseHandler)
    .use(router.routes())
    .use(router.allowedMethods());

export const printingsServer = serverless(app);
