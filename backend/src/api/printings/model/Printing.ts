import { BaseModel } from "../../../model/BaseModel";

export class Printing extends BaseModel {
    orders: number;
    rate: number;
}
