
import { IDataSource } from "../../../repository/IDataSource";
import { Printing } from "../model/Printing";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Update a Printing.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<Printing>) => {
    const result = await dataStore.update(ctx.request.body);
    if (result.error) {
        errorHandler.sendError(ctx, errorConstants.message.error_update_printing, 400);
    } else {
        ctx.body = result.data;
    }
};
