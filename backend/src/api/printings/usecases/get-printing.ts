
import { IDataSource } from "../../../repository/IDataSource";
import { Printing } from "../model/Printing";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Get a printing.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<Printing>) => {
  const result = await dataStore.get("id", ctx.id);
  if (result.error) {
    errorHandler.sendError(ctx, errorConstants.message.error_printing_not_found, 404);
  } else {
    ctx.body = result.data;
  }
};
