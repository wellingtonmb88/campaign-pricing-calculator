
import { IDataSource } from "../../../repository/IDataSource";
import { Printing } from "../model/Printing";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Remove a Printing.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<Printing>) => {
    const resultGet = await dataStore.get("id", ctx.params.id);
    if (resultGet.error || !resultGet.data) {
        errorHandler.sendError(ctx, errorConstants.message.error_printing_not_found, 404);
    } else {
        const printing = resultGet.data;
        const resultRemove = await dataStore.remove(printing);
        if (resultRemove.error || !resultRemove.data) {
            errorHandler.sendError(ctx, errorConstants.message.error_remove_printing, 400);
        } else {
            ctx.body = resultRemove.data;
        }
    }
};
