
import * as createPrinting from "./create-printing";
import * as deletePrinting from "./delete-printing";
import * as updatePrinting from "./update-printing";
import * as getAllPrintings from "./get-all-printings";
import * as getPrinting from "./get-printing";

export const create = createPrinting;
export const update = updatePrinting;
export const remove = deletePrinting.default;
export const getAll = getAllPrintings.default;
export const get = getPrinting.default;
