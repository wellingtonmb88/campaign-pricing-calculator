
import { IDataSource } from "../../../repository/IDataSource";
import { Printing } from "../model/Printing";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Create a printing.
 * @param ctx - koa context
 * @param dataStore - printing dataStore
 */
export const create = async (ctx, dataStore: IDataSource<Printing>) => {
    const result = await dataStore.create(ctx.request.body);
    if (result.error || !result.data) {
        errorHandler.sendError(ctx, errorConstants.message.error_create_printing, 400);
    } else {
        ctx.body = result.data;
    }
};
