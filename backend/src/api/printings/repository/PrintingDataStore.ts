
import { IDataBase } from "../../../repository/IDataBase";
import { IDataSource } from "../../../repository/IDataSource";
import { Printing } from "../model/Printing";
import * as updatePrintingParams from "./update-printing-params";

const uuid = require("uuid").v1;

const PARAMS_PRINTINGS = {
    TableName: process.env.TABLE_PRINTING,
};

/** Class representing a Printing datastore */
export class PrintingDataStore implements IDataSource<Printing> {

    /**
     * Constructor of the PrintingDataStore.
     * @param dbManager - The manager to handle Printing's database.
     */
    constructor(private dbManager: IDataBase<Printing>) {
    }

    /**
     * Retrieve all Printings by page and limit from db.
     * @param page - The page number to retrieve Printings.
     * @param limit  - The number of Printings to return.
     */
    async getAll(page: number, limit: number): Promise<{ error: boolean, data: Printing[] }> {
        return new Promise<{ error: boolean, data: Printing[] }>(async (resolve, reject) => {
            const result = await this.dbManager.getAll(page, limit, PARAMS_PRINTINGS);
            if (result.error || !result.data) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: result.data });
            }
        });
    }

    /**
     * Retrieve a Printing from db.
     * @param key - The key that represents the database's column name.
     * @param value - The value to find the in database.
     */
    async get(key: string, value: any): Promise<{ error: boolean, data: Printing }> {
        return new Promise<{ error: boolean, data: Printing }>(async (resolve, reject) => {
            const result = await this.dbManager.get(key, value, PARAMS_PRINTINGS);
            if (result.error || !result.data) {
                resolve({ error: true, data: null });
            } else {
                const printing = result.data;
                resolve({ error: false, data: printing });
            }
        });
    }

    /**
     * Remove a printing from db.
     * @param printing - Object that represents a printing model.
     */
    async remove(printing: Printing): Promise<{ error: boolean, data: Printing }> {
        if (printing) {
            printing.deleted = true;
        }
        const paramsUpdate = updatePrintingParams.default(printing, PARAMS_PRINTINGS);
        return new Promise<{ error: boolean, data: Printing }>(async (resolve, reject) => {
            const result = await this.dbManager.update(printing, paramsUpdate);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: printing });
            }
        });
    }

    /**
     * Update a printing in the db.
     * @param printing  - Object that represents a printing model.
     */
    async update(printing: Printing): Promise<{ error: boolean, data: Printing }> {
        const paramsUpdate = updatePrintingParams.default(printing, PARAMS_PRINTINGS);
        return new Promise<{ error: boolean, data: Printing }>(async (resolve, reject) => {
            const result = await this.dbManager.update(printing, paramsUpdate);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: printing });
            }
        });
    }

    /**
     * Create a printing in the db.
     * @param printing  - Object that represents a printing model.
     * @param shouldHashPwd - A Boolean to endicate that the password should be hashed.
     */
    async create(printing: Printing, shouldHashPwd: boolean): Promise<{ error: boolean, data: Printing }> {
        printing.id = uuid();
        printing.deleted = false;
        printing.createdAt = new Date().getTime();
        return new Promise<{ error: boolean, data: Printing }>(async (resolve, reject) => {
            const result = await this.dbManager.save(printing, PARAMS_PRINTINGS);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: printing });
            }
        });
    }
}
