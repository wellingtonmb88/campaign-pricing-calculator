
/**
 * Create a object with parameters to update a printing of db.
 * @param printing - Object that represents a printing model.
 * @param params - Parameters to be passed into dynamoDB.
 */
export default (printing, params) => {
    const expressionAttributeNames = {
    };

    const expressionAttributeValues = {
    };

    let updateExpression = "SET ";
    let hasFisrt = false;

    if (printing.orders) {
        expressionAttributeValues[":orders"] = printing.orders;
        expressionAttributeNames["#orders"] = "orders";
        updateExpression += "#orders = :orders ";
        hasFisrt = true;
    }
    if (printing.rate) {
        expressionAttributeValues[":rate"] = printing.rate;
        expressionAttributeNames["#rate"] = "rate";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#rate = :rate";
        hasFisrt = true;
    }
    if (printing.deleted) {
        expressionAttributeValues[":deleted"] = printing.deleted;
        expressionAttributeNames["#deleted"] = "deleted";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#deleted = :deleted";
        hasFisrt = true;
    }
    return {
        TableName: params.TableName,
        Key: { id: printing.id },
        ExpressionAttributeNames: expressionAttributeNames,
        ExpressionAttributeValues: expressionAttributeValues,
        UpdateExpression: updateExpression,
        ReturnValues: "ALL_NEW",
    };
};
