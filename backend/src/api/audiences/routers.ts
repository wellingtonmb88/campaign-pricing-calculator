
import * as Koa from "koa";
import * as koaBody from "koa-body";
import * as authorizer from "../../middleware/authorizer";
import * as audienceUseCase from "./usecases/index";
import { responseHandler } from "../../middleware/response-handler";
import { DynamoDbManager } from "../../repository/DynamoDbManager";
import { AudienceDataStore } from "./repository/AudienceDataStore";
import { graphqlKoa, graphiqlKoa } from "apollo-server-koa";
import { makeExecutableSchema } from "graphql-tools";

const serverless = require("serverless-http");
const Router = require("koa-router");
const router = new Router();
const app = new Koa();

const dataBaseManager = new DynamoDbManager();
let dataStore = new AudienceDataStore(dataBaseManager);

export const setupTest = (mockAudienceDataBaseManager) => {
    dataStore = new AudienceDataStore(mockAudienceDataBaseManager);
};

// The GraphQL schema in string form
const typeDefs = `
    """
    Input represanting an Audience object
    """
    input AudienceInput {
        name      : String!
        username  : String!
        email     : String!
        password  : String!
        role      : String!
    }
    """
    Input represanting an Audience object to be updated
    """
    input UpdateAudienceInput {
        id          : String!
        name        : String
        username    : String
        email       : String
        password    : String
        role        : String
    }
    """
    Model represanting a Audience object
    """
    type Audience {
        id          : String
        name        : String
        username    : String
        email       : String
        role        : String
        deleted     : Boolean
        createdAt   : Float
    }
    """
    Object to format the response of pagination query
    """
    type Items {
        items: [Audience]
        count: Int
        page: Int
    }
    """
    Querying Audience
    """
    type Query {
        """
        Search Industries with pagination
        """
        audiences(page: Int!, limit: Int): Items,
        """
        Search Audience by Id
        """
        audience(id: String!): Audience
    }
    """
    Mutating Audience
    """
    type Mutation {
        """
        Create a new Audience
        """
        createAudience (newAudience: AudienceInput!): Audience,
        """
        Update an Audience
        """
        updateAudience (audience: UpdateAudienceInput!): Audience,
        """
        Delete an Audience
        """
        deleteAudience (id: String!): Audience
    }
`;

const resolvers = {
    Mutation: {
        async createAudience(_, { newAudience }, ctx) {
            ctx.request.body = newAudience;
            await audienceUseCase.create.create(ctx, dataStore);
            return ctx.body;
        },
        async updateAudience(_, { audience }, ctx) {
            ctx.request.body = audience;
            await audienceUseCase.update.default(ctx, dataStore);
            return ctx.body;
        },
        async deleteAudience(_, { id }, ctx) {
            ctx.params.id = id;
            await audienceUseCase.remove(ctx, dataStore);
            return ctx.body;
        },
    },
    Query: {
        async audiences(_, { page, limit }, ctx) {
            ctx.query.page = page;
            ctx.query.limit = limit;
            await audienceUseCase.getAll(ctx, dataStore);
            return ctx.body;
        },
        async audience(_, { id }, ctx) {
            ctx.id = id;
            await audienceUseCase.get(ctx, dataStore);
            return ctx.body;
        },
    },
};

const logger = { log: e => console.log(e) };

const schemas = makeExecutableSchema({
    logger,
    resolvers,
    typeDefs,
});

const ENDPOINT = "/audiences/graphql";

router.post(ENDPOINT,
    authorizer.authScope({ admin: true, support: false, common: false }),
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

router.get(ENDPOINT,
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

if (process.env.ENABLE_GRAPHIQL) {
    router.get("/audiences/graphiql", graphiqlKoa({ endpointURL: ENDPOINT }));
}

app.use(koaBody())
    .use(responseHandler)
    .use(router.routes())
    .use(router.allowedMethods());

export const audiencesServer = serverless(app);
