
/**
 * Create a object with parameters to update a audience of db.
 * @param audience - Object that represents a audience model.
 * @param params - Parameters to be passed into dynamoDB.
 */
export default (audience, params) => {
    const expressionAttributeNames = {
    };

    const expressionAttributeValues = {
    };

    let updateExpression = "SET ";
    let hasFisrt = false;

    if (audience.name) {
        expressionAttributeValues[":name"] = audience.name;
        expressionAttributeNames["#name"] = "name";
        updateExpression += "#name = :name ";
        hasFisrt = true;
    }
    if (audience.username) {
        expressionAttributeValues[":username"] = audience.username;
        expressionAttributeNames["#username"] = "username";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#username = :username";
        hasFisrt = true;
    }
    if (audience.email) {
        expressionAttributeValues[":email"] = audience.email;
        expressionAttributeNames["#email"] = "email";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#email = :email";
        hasFisrt = true;
    }
    if (audience.password) {
        expressionAttributeValues[":password"] = audience.password;
        expressionAttributeNames["#password"] = "password";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#password = :password";
        hasFisrt = true;
    }
    if (audience.deleted) {
        expressionAttributeValues[":deleted"] = audience.deleted;
        expressionAttributeNames["#deleted"] = "deleted";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#deleted = :deleted";
        hasFisrt = true;
    }
    return {
        TableName: params.TableName,
        Key: { id: audience.id },
        ExpressionAttributeNames: expressionAttributeNames,
        ExpressionAttributeValues: expressionAttributeValues,
        UpdateExpression: updateExpression,
        ReturnValues: "ALL_NEW",
    };
};
