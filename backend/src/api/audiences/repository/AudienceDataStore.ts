
import { IDataBase } from "../../../repository/IDataBase";
import { IDataSource } from "../../../repository/IDataSource";
import { Audience } from "../model/Audience";
import * as credentialsHandler from "../../../utils/credentials-handler";
import * as updateAudienceParams from "./update-audience-params";

const uuid = require("uuid").v1;

const PARAMS_AUDIENCES = {
    TableName: process.env.TABLE_AUDIENCE,
};

/** Class representing an Audience datastore */
export class AudienceDataStore implements IDataSource<Audience> {

    /**
     * Constructor of the AudienceDataStore.
     * @param dbManager - The manager to handle Audience's database.
     */
    constructor(private dbManager: IDataBase<Audience>) {
    }

    /**
     * Retrieve all Audiences by page and limit from db.
     * @param page - The page number to retrieve Audiences.
     * @param limit  - The number of Audiences to return.
     */
    async getAll(page: number, limit: number): Promise<{ error: boolean, data: Audience[] }> {
        return new Promise<{ error: boolean, data: Audience[] }>(async (resolve, reject) => {
            const result = await this.dbManager.getAll(page, limit, PARAMS_AUDIENCES);
            if (result.error || !result.data) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: result.data });
            }
        });
    }

    /**
     * Retrieve an Audience from db.
     * @param key - The key that represents the database's column name.
     * @param value - The value to find the in database.
     */
    async get(key: string, value: any): Promise<{ error: boolean, data: Audience }> {
        return new Promise<{ error: boolean, data: Audience }>(async (resolve, reject) => {
            const result = await this.dbManager.get(key, value, PARAMS_AUDIENCES);
            if (result.error || !result.data) {
                resolve({ error: true, data: null });
            } else {
                const audience = result.data;
                resolve({ error: false, data: audience });
            }
        });
    }

    /**
     * Remove an Audience from db.
     * @param audience - Object that represents an Audience model.
     */
    async remove(audience: Audience): Promise<{ error: boolean, data: Audience }> {
        if (audience) {
            audience.deleted = true;
        }
        const paramsUpdate = updateAudienceParams.default(audience, PARAMS_AUDIENCES);
        return new Promise<{ error: boolean, data: Audience }>(async (resolve, reject) => {
            const result = await this.dbManager.update(audience, paramsUpdate);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: audience });
            }
        });
    }

    /**
     * Update an Audience in the db.
     * @param audience  - Object that represents an Audience model.
     */
    async update(audience: Audience): Promise<{ error: boolean, data: Audience }> {
        const paramsUpdate = updateAudienceParams.default(audience, PARAMS_AUDIENCES);
        return new Promise<{ error: boolean, data: Audience }>(async (resolve, reject) => {
            const result = await this.dbManager.update(audience, paramsUpdate);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: audience });
            }
        });
    }

    /**
     * Create an Audience in the db.
     * @param audience  - Object that represents an Audience model.
     * @param shouldHashPwd - A Boolean to endicate that the password should be hashed.
     */
    async create(audience: Audience, shouldHashPwd: boolean): Promise<{ error: boolean, data: Audience }> {
        audience.id = uuid();
        audience.deleted = false;
        audience.createdAt = new Date().getTime();
        if (shouldHashPwd && audience.password) {
            audience.password = await credentialsHandler.hashPassword(audience.password);
        }
        return new Promise<{ error: boolean, data: Audience }>(async (resolve, reject) => {
            const result = await this.dbManager.save(audience, PARAMS_AUDIENCES);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: audience });
            }
        });
    }
}
