
import { IDataSource } from "../../../repository/IDataSource";
import { Audience } from "../model/Audience";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Update an Audience.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<Audience>) => {
    const result = await dataStore.update(ctx.request.body);
    if (result.error) {
        errorHandler.sendError(ctx, errorConstants.message.error_update_audience, 400);
    } else {
        ctx.body = result.data;
    }
};
