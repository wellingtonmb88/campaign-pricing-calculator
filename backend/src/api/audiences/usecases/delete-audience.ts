
import { IDataSource } from "../../../repository/IDataSource";
import { Audience } from "../model/Audience";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Remove an Audience.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<Audience>) => {
    const resultGet = await dataStore.get("id", ctx.params.id);
    if (resultGet.error || !resultGet.data) {
        errorHandler.sendError(ctx, errorConstants.message.error_audience_not_found, 404);
    } else {
        const audience = resultGet.data;
        const resultRemove = await dataStore.remove(audience);
        if (resultRemove.error || !resultRemove.data) {
            errorHandler.sendError(ctx, errorConstants.message.error_remove_audience, 400);
        } else {
            ctx.body = resultRemove.data;
        }
    }
};
