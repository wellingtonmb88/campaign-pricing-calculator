
import { IDataSource } from "../../../repository/IDataSource";
import { Audience } from "../model/Audience";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";
import * as credentialsHandler from "../../../utils/credentials-handler";

/**
 * Create an audience.
 * @param ctx - koa context
 * @param dataStore - audience dataStore
 */
export const create = async (ctx, dataStore: IDataSource<Audience>) => {
    const userNotExists = await credentialsHandler.verifyIfUserNotExist(ctx, dataStore);
    if (userNotExists) {
        const result = await dataStore.create(ctx.request.body, true);
        if (result.error || !result.data) {
            errorHandler.sendError(ctx, errorConstants.message.error_create_audience, 400);
        } else {
            ctx.body = result.data;
        }
    }
};
