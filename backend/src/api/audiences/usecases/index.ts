
import * as createAudience from "./create-audience";
import * as deleteAudience from "./delete-audience";
import * as updateAudience from "./update-audience";
import * as getAllAudiences from "./get-all-audiences";
import * as getAudience from "./get-audience";

export const create = createAudience;
export const update = updateAudience;
export const remove = deleteAudience.default;
export const getAll = getAllAudiences.default;
export const get = getAudience.default;
