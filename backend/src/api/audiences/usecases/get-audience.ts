
import { IDataSource } from "../../../repository/IDataSource";
import { Audience } from "../model/Audience";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Get an Audience.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<Audience>) => {
  const result = await dataStore.get("id", ctx.id);
  if (result.error) {
    errorHandler.sendError(ctx, errorConstants.message.error_audience_not_found, 404);
  } else {
    ctx.body = result.data;
  }
};
