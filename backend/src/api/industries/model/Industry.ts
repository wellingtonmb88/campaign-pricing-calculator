import { BaseModel } from "../../../model/BaseModel";

export class Industry extends BaseModel {
    name: string;
    cvr: number;
}
