
import * as Koa from "koa";
import * as koaBody from "koa-body";
import * as authorizer from "../../middleware/authorizer";
import * as industryUseCase from "./usecases/index";
import { responseHandler } from "../../middleware/response-handler";
import { DynamoDbManager } from "../../repository/DynamoDbManager";
import { IndustryDataStore } from "./repository/IndustryDataStore";
import { graphqlKoa, graphiqlKoa } from "apollo-server-koa";
import { makeExecutableSchema } from "graphql-tools";

const serverless = require("serverless-http");
const Router = require("koa-router");
const router = new Router();
const app = new Koa();

const dataBaseManager = new DynamoDbManager();
let dataStore = new IndustryDataStore(dataBaseManager);

export const setupTest = (mockIndustryDataBaseManager) => {
    dataStore = new IndustryDataStore(mockIndustryDataBaseManager);
};

// The GraphQL schema in string form
const typeDefs = `
    """
    Input represanting an Industry object
    """
    input IndustryInput {
        id          : String
        name        : String!
        cvr         : Float!
    }
    """
    Input represanting an Industry object to be updated
    """
    input UpdateIndustryInput {
        id          : String!
        name        : String
        cvr         : Float
    }
    """
    Model represanting an Industry object
    """
    type Industry {
        id          : String
        name        : String
        cvr         : Float
        deleted     : Boolean
        createdAt   : Float
    }
    """
    Object to format the response of pagination query
    """
    type Items {
        items: [Industry]
        count: Int
        page: Int
    }
    """
    Querying Industry
    """
    type Query {
        """
        Search Industries with pagination
        """
        industries(page: Int!, limit: Int): Items,
        """
        Search Industry by Id
        """
        industry(id: String!): Industry
    }
    """
    Mutating Industry
    """
    type Mutation {
        """
        Create a new Industry
        """
        createIndustry (newIndustry: IndustryInput!): Industry,
        """
        Update an Industry
        """
        updateIndustry (industry: UpdateIndustryInput!): Industry,
        """
        Delete an Industry
        """
        deleteIndustry (id: String!): Industry
    }
`;

const resolvers = {
    Mutation: {
        async createIndustry(_, { newIndustry }, ctx) {
            ctx.request.body = newIndustry;
            await industryUseCase.create.create(ctx, dataStore);
            return ctx.body;
        },
        async updateIndustry(_, { industry }, ctx) {
            ctx.request.body = industry;
            await industryUseCase.update.default(ctx, dataStore);
            return ctx.body;
        },
        async deleteIndustry(_, { id }, ctx) {
            ctx.params.id = id;
            await industryUseCase.remove(ctx, dataStore);
            return ctx.body;
        },
    },
    Query: {
        async industries(_, { page, limit }, ctx) {
            ctx.query.page = page;
            ctx.query.limit = limit;
            await industryUseCase.getAll(ctx, dataStore);
            return ctx.body;
        },
        async industry(_, { id }, ctx) {
            ctx.id = id;
            await industryUseCase.get(ctx, dataStore);
            return ctx.body;
        },
    },
};

const schemas = makeExecutableSchema({
    resolvers,
    typeDefs,
});

const ENDPOINT = "/industries/graphql";

router.post(ENDPOINT,
    authorizer.authScope({ admin: true, support: true, common: false }),
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

router.get(ENDPOINT,
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

if (process.env.ENABLE_GRAPHIQL) {
    router.get("/industries/graphiql", graphiqlKoa({ endpointURL: ENDPOINT }));
}

app.use(koaBody())
    .use(responseHandler)
    .use(router.routes())
    .use(router.allowedMethods());

export const industriesServer = serverless(app);
