
import { IDataBase } from "../../../repository/IDataBase";
import { IDataSource } from "../../../repository/IDataSource";
import { Industry } from "../model/Industry";
import * as updateIndustryParams from "./update-industry-params";

const uuid = require("uuid").v1;

const PARAMS_INDUSTRIES = {
    TableName: process.env.TABLE_INDUSTRY,
};

/** Class representing an Industry datastore */
export class IndustryDataStore implements IDataSource<Industry> {

    /**
     * Constructor of the IndustryDataStore.
     * @param dbManager - The manager to handle Industry's database.
     */
    constructor(private dbManager: IDataBase<Industry>) {
    }

    /**
     * Retrieve all Industries by page and limit from db.
     * @param page - The page number to retrieve Industries.
     * @param limit  - The number of Industries to return.
     */
    async getAll(page: number, limit: number): Promise<{ error: boolean, data: Industry[] }> {
        return new Promise<{ error: boolean, data: Industry[] }>(async (resolve, reject) => {
            const result = await this.dbManager.getAll(page, limit, PARAMS_INDUSTRIES);
            if (result.error || !result.data) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: result.data });
            }
        });
    }

    /**
     * Retrieve an Industry from db.
     * @param key - The key that represents the database's column name.
     * @param value - The value to find the in database.
     */
    async get(key: string, value: any): Promise<{ error: boolean, data: Industry }> {
        return new Promise<{ error: boolean, data: Industry }>(async (resolve, reject) => {
            const result = await this.dbManager.get(key, value, PARAMS_INDUSTRIES);
            if (result.error || !result.data) {
                resolve({ error: true, data: null });
            } else {
                const industry = result.data;
                resolve({ error: false, data: industry });
            }
        });
    }

    /**
     * Remove an Industry from db.
     * @param industry - Object that represents an Industry model.
     */
    async remove(industry: Industry): Promise<{ error: boolean, data: Industry }> {
        if (industry) {
            industry.deleted = true;
        }
        const paramsUpdate = updateIndustryParams.default(industry, PARAMS_INDUSTRIES);
        return new Promise<{ error: boolean, data: Industry }>(async (resolve, reject) => {
            const result = await this.dbManager.update(industry, paramsUpdate);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: industry });
            }
        });
    }

    /**
     * Update an Industry in the db.
     * @param industry  - Object that represents an Industry model.
     */
    async update(industry: Industry): Promise<{ error: boolean, data: Industry }> {
        const paramsUpdate = updateIndustryParams.default(industry, PARAMS_INDUSTRIES);
        return new Promise<{ error: boolean, data: Industry }>(async (resolve, reject) => {
            const result = await this.dbManager.update(industry, paramsUpdate);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: industry });
            }
        });
    }

    /**
     * Create an Industry in the db.
     * @param industry  - Object that represents an Industry model.
     * @param shouldHashPwd - A Boolean to endicate that the password should be hashed.
     */
    async create(industry: Industry, shouldHashPwd: boolean): Promise<{ error: boolean, data: Industry }> {
        industry.id = uuid();
        industry.deleted = false;
        industry.createdAt = new Date().getTime();
        return new Promise<{ error: boolean, data: Industry }>(async (resolve, reject) => {
            const result = await this.dbManager.save(industry, PARAMS_INDUSTRIES);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: industry });
            }
        });
    }
}
