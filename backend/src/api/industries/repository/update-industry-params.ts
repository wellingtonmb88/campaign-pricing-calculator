
/**
 * Create a object with parameters to update a industry of db.
 * @param industry - Object that represents a industry model.
 * @param params - Parameters to be passed into dynamoDB.
 */
export default (industry, params) => {
    const expressionAttributeNames = {
    };

    const expressionAttributeValues = {
    };

    let updateExpression = "SET ";
    let hasFisrt = false;

    if (industry.name) {
        expressionAttributeValues[":name"] = industry.name;
        expressionAttributeNames["#name"] = "name";
        updateExpression += "#name = :name ";
        hasFisrt = true;
    }
    if (industry.cvr) {
        expressionAttributeValues[":cvr"] = industry.cvr;
        expressionAttributeNames["#cvr"] = "cvr";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#cvr = :cvr";
        hasFisrt = true;
    }
    if (industry.deleted) {
        expressionAttributeValues[":deleted"] = industry.deleted;
        expressionAttributeNames["#deleted"] = "deleted";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#deleted = :deleted";
        hasFisrt = true;
    }
    return {
        TableName: params.TableName,
        Key: { id: industry.id },
        ExpressionAttributeNames: expressionAttributeNames,
        ExpressionAttributeValues: expressionAttributeValues,
        UpdateExpression: updateExpression,
        ReturnValues: "ALL_NEW",
    };
};
