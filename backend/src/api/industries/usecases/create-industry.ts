
import { IDataSource } from "../../../repository/IDataSource";
import { Industry } from "../model/Industry";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Create an industry.
 * @param ctx - koa context
 * @param dataStore - industry dataStore
 */
export const create = async (ctx, dataStore: IDataSource<Industry>) => {
    const result = await dataStore.create(ctx.request.body);
    if (result.error || !result.data) {
        errorHandler.sendError(ctx, errorConstants.message.error_create_industry, 400);
    } else {
        ctx.body = result.data;
    }
};
