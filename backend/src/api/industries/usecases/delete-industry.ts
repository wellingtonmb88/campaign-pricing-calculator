
import { IDataSource } from "../../../repository/IDataSource";
import { Industry } from "../model/Industry";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Remove an Industry.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<Industry>) => {
    const resultGet = await dataStore.get("id", ctx.params.id);
    if (resultGet.error || !resultGet.data) {
        errorHandler.sendError(ctx, errorConstants.message.error_industry_not_found, 404);
    } else {
        const industry = resultGet.data;
        const resultRemove = await dataStore.remove(industry);
        if (resultRemove.error || !resultRemove.data) {
            errorHandler.sendError(ctx, errorConstants.message.error_remove_industry, 400);
        } else {
            ctx.body = resultRemove.data;
        }
    }
};
