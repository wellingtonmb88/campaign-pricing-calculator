
import { IDataSource } from "../../../repository/IDataSource";
import { Industry } from "../model/Industry";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Get an Industry.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<Industry>) => {
  const result = await dataStore.get("id", ctx.id);
  if (result.error) {
    errorHandler.sendError(ctx, errorConstants.message.error_industry_not_found, 404);
  } else {
    ctx.body = result.data;
  }
};
