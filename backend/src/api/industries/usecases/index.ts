
import * as createIndustry from "./create-industry";
import * as deleteIndustry from "./delete-industry";
import * as updateIndustry from "./update-industry";
import * as getAllIndustries from "./get-all-industries";
import * as getIndustry from "./get-industry";

export const create = createIndustry;
export const update = updateIndustry;
export const remove = deleteIndustry.default;
export const getAll = getAllIndustries.default;
export const get = getIndustry.default;
