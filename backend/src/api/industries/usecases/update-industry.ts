
import { IDataSource } from "../../../repository/IDataSource";
import { Industry } from "../model/Industry";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Update an Industry.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<Industry>) => {
    const result = await dataStore.update(ctx.request.body);
    if (result.error) {
        errorHandler.sendError(ctx, errorConstants.message.error_update_industry, 400);
    } else {
        ctx.body = result.data;
    }
};
