
import * as Koa from "koa";
import * as koaBody from "koa-body";
import { responseHandler } from "../../middleware/response-handler";
import { graphqlKoa, graphiqlKoa } from "apollo-server-koa";
import { makeExecutableSchema } from "graphql-tools";

const serverless = require("serverless-http");
const Router = require("koa-router");
const router = new Router();
const app = new Koa();

// The GraphQL schema in string form
const typeDefs = `
    type Result {
        totalCampaign: Float!,
        costPerAcquisition: Float!,
        numberOfAcquisitions: Float!
    }
    """
    Input represanting a Distribution object
    """
    input Distribution {
        fliersPerMission    : Int
        costPerMission      : Float
    }
    """
    Input represanting a Printing object
    """
    input Printing {
        orders      : Int
        rate        : Float
    }
    """
    Input represanting a City object
    """
    input City {
        distribution    : Distribution!
        purpose         : Int!
    }
    """
    Query Calculate Campaign
    """
    type Query {
        """
        Calculate total of campaign, number of acquisitions and cost per acquisition
        """
        calculate(cities: [City]!, printing: Printing!, conversionRate: Float!): Result!
    }
`;

const resolvers = {
    Query: {
        async calculate(_, { cities, printing, conversionRate }, ctx) {

            let totalCampaign = 0;
            let numberOfAcquisitions = 0;
            let costPerAcquisition = 0;

            cities.forEach(city => {
                totalCampaign +=
                    (((city.purpose / city.distribution.fliersPerMission) * city.distribution.costPerMission) +
                        (printing.orders * printing.rate));

                numberOfAcquisitions += (city.purpose * (conversionRate / 100));

                costPerAcquisition += totalCampaign / numberOfAcquisitions;
            });

            return {
                totalCampaign: parseFloat(totalCampaign.toString()).toFixed(2),
                numberOfAcquisitions,
                costPerAcquisition: parseFloat(costPerAcquisition.toString()).toFixed(2),
            };
        },
    },
};

const schemas = makeExecutableSchema({
    resolvers,
    typeDefs,
});

const ENDPOINT = "/calculations/graphql";

router.post(ENDPOINT,
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

if (process.env.ENABLE_GRAPHIQL) {
    router.get("/calculations/graphiql", graphiqlKoa({ endpointURL: ENDPOINT }));
}

app.use(koaBody())
    .use(responseHandler)
    .use(router.routes())
    .use(router.allowedMethods());

export const calculationsServer = serverless(app);
