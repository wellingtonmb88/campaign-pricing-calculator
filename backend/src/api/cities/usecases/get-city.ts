
import { IDataSource } from "../../../repository/IDataSource";
import { City } from "../model/City";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Get a city.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<City>) => {
  const result = await dataStore.get("id", ctx.id);
  if (result.error) {
    errorHandler.sendError(ctx, errorConstants.message.error_city_not_found, 404);
  } else {
    ctx.body = result.data;
  }
};
