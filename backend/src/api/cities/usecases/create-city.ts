
import { IDataSource } from "../../../repository/IDataSource";
import { City } from "../model/City";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Create a city.
 * @param ctx - koa context
 * @param dataStore - city dataStore
 */
export const create = async (ctx, dataStore: IDataSource<City>) => {
    const result = await dataStore.create(ctx.request.body);
    if (result.error || !result.data) {
        errorHandler.sendError(ctx, errorConstants.message.error_create_city, 400);
    } else {
        ctx.body = result.data;
    }
};
