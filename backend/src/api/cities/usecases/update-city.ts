
import { IDataSource } from "../../../repository/IDataSource";
import { City } from "../model/City";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Update a City.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<City>) => {
    const result = await dataStore.update(ctx.request.body);
    if (result.error) {
        errorHandler.sendError(ctx, errorConstants.message.error_update_city, 400);
    } else {
        ctx.body = result.data;
    }
};
