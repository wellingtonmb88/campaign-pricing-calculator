
import { IDataSource } from "../../../repository/IDataSource";
import { City } from "../model/City";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Get all Citys.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<City>) => {
  let page = 1;
  let limit = 5;
  if (ctx.query.page) {
    page = ctx.query.page;
  }
  if (ctx.query.limit) {
    limit = ctx.query.limit;
  }
  const result = await dataStore.getAll(page, limit);
  if (result.error || !result.data) {
    errorHandler.sendError(ctx, errorConstants.message.error_cities_not_found, 404);
  } else {
    ctx.body = result.data;
  }
};
