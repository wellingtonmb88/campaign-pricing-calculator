
import { IDataSource } from "../../../repository/IDataSource";
import { City } from "../model/City";
import * as errorConstants from "../../../utils/error-constants";
import * as errorHandler from "../../../utils/error-handler";

/**
 * Remove a City.
 * @param ctx - koa context
 * @param dataStore - dataStore
 */
export default async (ctx, dataStore: IDataSource<City>) => {
    const resultGet = await dataStore.get("id", ctx.params.id);
    if (resultGet.error || !resultGet.data) {
        errorHandler.sendError(ctx, errorConstants.message.error_city_not_found, 404);
    } else {
        const city = resultGet.data;
        const resultRemove = await dataStore.remove(city);
        if (resultRemove.error || !resultRemove.data) {
            errorHandler.sendError(ctx, errorConstants.message.error_remove_city, 400);
        } else {
            ctx.body = resultRemove.data;
        }
    }
};
