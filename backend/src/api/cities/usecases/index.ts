
import * as createCity from "./create-city";
import * as deleteCity from "./delete-city";
import * as updateCity from "./update-city";
import * as getAllCities from "./get-all-cities";
import * as getCity from "./get-city";

export const create = createCity;
export const update = updateCity;
export const remove = deleteCity.default;
export const getAll = getAllCities.default;
export const get = getCity.default;
