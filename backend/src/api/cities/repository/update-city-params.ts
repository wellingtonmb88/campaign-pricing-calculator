
/**
 * Create a object with parameters to update a city of db.
 * @param city - Object that represents a city model.
 * @param params - Parameters to be passed into dynamoDB.
 */
export default (city, params) => {
    const expressionAttributeNames = {
    };

    const expressionAttributeValues = {
    };

    let updateExpression = "SET ";
    let hasFisrt = false;

    if (city.name) {
        expressionAttributeValues[":name"] = city.name;
        expressionAttributeNames["#name"] = "name";
        updateExpression += "#name = :name ";
        hasFisrt = true;
    }
    if (city.distribution) {
        expressionAttributeValues[":distribution"] = city.distribution;
        expressionAttributeNames["#distribution"] = "distribution";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#distribution = :distribution";
        hasFisrt = true;
    }
    if (city.purpose) {
        expressionAttributeValues[":purpose"] = city.purpose;
        expressionAttributeNames["#purpose"] = "purpose";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#purpose = :purpose";
        hasFisrt = true;
    }
    if (city.latitude) {
        expressionAttributeValues[":latitude"] = city.latitude;
        expressionAttributeNames["#latitude"] = "latitude";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#latitude = :latitude";
        hasFisrt = true;
    }
    if (city.longitude) {
        expressionAttributeValues[":longitude"] = city.longitude;
        expressionAttributeNames["#longitude"] = "longitude";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#longitude = :longitude";
        hasFisrt = true;
    }
    if (city.deleted) {
        expressionAttributeValues[":deleted"] = city.deleted;
        expressionAttributeNames["#deleted"] = "deleted";
        updateExpression += hasFisrt ? " , " : "  ";
        updateExpression += "#deleted = :deleted";
        hasFisrt = true;
    }
    return {
        TableName: params.TableName,
        Key: { id: city.id },
        ExpressionAttributeNames: expressionAttributeNames,
        ExpressionAttributeValues: expressionAttributeValues,
        UpdateExpression: updateExpression,
        ReturnValues: "ALL_NEW",
    };
};
