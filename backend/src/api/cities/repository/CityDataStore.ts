
import { IDataBase } from "../../../repository/IDataBase";
import { IDataSource } from "../../../repository/IDataSource";
import { City } from "../model/City";
import * as updateCityParams from "./update-city-params";

const uuid = require("uuid").v1;

const PARAMS_CITY = {
    TableName: process.env.TABLE_CITY,
};

/** Class representing a City datastore */
export class CityDataStore implements IDataSource<City> {

    /**
     * Constructor of the CityDataStore.
     * @param dbManager - The manager to handle City's database.
     */
    constructor(private dbManager: IDataBase<City>) {
    }

    /**
     * Retrieve all Citys by page and limit from db.
     * @param page - The page number to retrieve Citys.
     * @param limit  - The number of Citys to return.
     */
    async getAll(page: number, limit: number): Promise<{ error: boolean, data: City[] }> {
        return new Promise<{ error: boolean, data: City[] }>(async (resolve, reject) => {
            const result = await this.dbManager.getAll(page, limit, PARAMS_CITY);
            if (result.error || !result.data) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: result.data });
            }
        });
    }

    /**
     * Retrieve a City from db.
     * @param key - The key that represents the database's column name.
     * @param value - The value to find the in database.
     */
    async get(key: string, value: any): Promise<{ error: boolean, data: City }> {
        return new Promise<{ error: boolean, data: City }>(async (resolve, reject) => {
            const result = await this.dbManager.get(key, value, PARAMS_CITY);
            if (result.error || !result.data) {
                resolve({ error: true, data: null });
            } else {
                const city = result.data;
                resolve({ error: false, data: city });
            }
        });
    }

    /**
     * Remove a city from db.
     * @param city - Object that represents a city model.
     */
    async remove(city: City): Promise<{ error: boolean, data: City }> {
        if (city) {
            city.deleted = true;
        }
        const paramsUpdate = updateCityParams.default(city, PARAMS_CITY);
        return new Promise<{ error: boolean, data: City }>(async (resolve, reject) => {
            const result = await this.dbManager.update(city, paramsUpdate);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: city });
            }
        });
    }

    /**
     * Update a city in the db.
     * @param city  - Object that represents a city model.
     */
    async update(city: City): Promise<{ error: boolean, data: City }> {
        const paramsUpdate = updateCityParams.default(city, PARAMS_CITY);
        return new Promise<{ error: boolean, data: City }>(async (resolve, reject) => {
            const result = await this.dbManager.update(city, paramsUpdate);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: city });
            }
        });
    }

    /**
     * Create a city in the db.
     * @param city  - Object that represents a city model.
     * @param shouldHashPwd - A Boolean to endicate that the password should be hashed.
     */
    async create(city: City, shouldHashPwd: boolean): Promise<{ error: boolean, data: City }> {
        city.id = uuid();
        city.deleted = false;
        city.createdAt = new Date().getTime();
        return new Promise<{ error: boolean, data: City }>(async (resolve, reject) => {
            const result = await this.dbManager.save(city, PARAMS_CITY);
            if (result.error || !result.success) {
                resolve({ error: true, data: null });
            } else {
                resolve({ error: false, data: city });
            }
        });
    }
}
