import { Distribution } from "./Distribution";
import { Purpose } from "./Purpose";
import { BaseModel } from "../../../model/BaseModel";

export class City extends BaseModel {
    name: number;
    distribution: Distribution;
    purpose: Purpose;
    latitude: number;
    longitude: number;
}
