
export const typeCity = `
    """
    Model represanting a City object
    """
    type City {
        id          : String
        name        : String
        distribution : Distribution
        purpose     : Purpose
        latitude    : Float
        longitude   : Float
        deleted     : Boolean
        createdAt   : Float
    }
    """
    Input represanting a City object
    """
    input CityInput {
        id          : String
        name        : String!
        distribution: DistributionInput!
        purpose     : PurposeInput!
        latitude    : Float!
        longitude    : Float!
        deleted     : Boolean
        createdAt   : Float
    }
    """
    Input represanting a City object to be updated
    """
    input UpdateCityInput {
        id          : String!
        name        : String
        distribution: DistributionInput
        purpose     : PurposeInput
        latitude    : Float
        longitude   : Float
        deleted     : Boolean
        createdAt   : Float
    }
`;
