
export const typeDistribution = `
    """
    Model represanting a Distribution object
    """
    type Distribution {
        fliersPerMission    : Int
        costPerMission      : Float
    }
    """
    Input represanting a Distribution object
    """
    input DistributionInput {
        fliersPerMission    : Int!
        costPerMission      : Float!
    }
`;
