
export const typePurpose = `
    """
    Model represanting a Purpose object
    """
    type Purpose {
        test          : Int
        validate      : Int
        growth        : Int
        scaleUp       : Int
    }
    """
    Input represanting a Purpose object
    """
    input PurposeInput {
        test          : Int!
        validate      : Int!
        growth        : Int!
        scaleUp       : Int!
    }
`;
