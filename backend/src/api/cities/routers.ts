
import * as Koa from "koa";
import * as koaBody from "koa-body";
import * as authorizer from "../../middleware/authorizer";
import * as cityUseCase from "./usecases/index";
import { responseHandler } from "../../middleware/response-handler";
import { DynamoDbManager } from "../../repository/DynamoDbManager";
import { CityDataStore } from "./repository/CityDataStore";
import { graphqlKoa, graphiqlKoa } from "apollo-server-koa";
import { makeExecutableSchema } from "graphql-tools";
import { typeCity } from "./graphql/type-city";
import { typeDistribution } from "./graphql/type-distribution";
import { typePurpose } from "./graphql/type-purpose";

const serverless = require("serverless-http");
const Router = require("koa-router");
const router = new Router();
const app = new Koa();

const dataBaseManager = new DynamoDbManager();
let dataStore = new CityDataStore(dataBaseManager);

export const setupTest = (mockCityDataBaseManager) => {
    dataStore = new CityDataStore(mockCityDataBaseManager);
};

// The GraphQL schema in string form
const typeDefs = `
    """
    Object to format the response of pagination query
    """
    type Items {
        items: [City]
        count: Int
        page: Int
    }
    """
    Querying City
    """
    type Query {
        """
        Search Cities with pagination
        """
        cities(page: Int!, limit: Int): Items,
        """
        Search City by Id
        """
        city(id: String!): City
    }
    """
    Mutating City
    """
    type Mutation {
        """
        Create a new City
        """
        createCity (newCity: CityInput!): City,
        """
        Update a City
        """
        updateCity (city: UpdateCityInput!): City,
        """
        Delete a City
        """
        deleteCity (id: String!): City
    }
`;

const resolvers = {
    Mutation: {
        async createCity(_, { newCity }, ctx) {
            ctx.request.body = newCity;
            await cityUseCase.create.create(ctx, dataStore);
            return ctx.body;
        },
        async updateCity(_, { city }, ctx) {
            ctx.request.body = city;
            await cityUseCase.update.default(ctx, dataStore);
            return ctx.body;
        },
        async deleteCity(_, { id }, ctx) {
            ctx.params.id = id;
            await cityUseCase.remove(ctx, dataStore);
            return ctx.body;
        },
    },
    Query: {
        async cities(_, { page, limit }, ctx) {
            ctx.query.page = page;
            ctx.query.limit = limit;
            await cityUseCase.getAll(ctx, dataStore);
            return ctx.body;
        },
        async city(_, { id }, ctx) {
            ctx.id = id;
            await cityUseCase.get(ctx, dataStore);
            return ctx.body;
        },
    },
};

const schemas = makeExecutableSchema({
    resolvers,
    typeDefs: [typeCity, typeDistribution, typePurpose, typeDefs],
});

const ENDPOINT = "/cities/graphql";

router.post(ENDPOINT,
    authorizer.authScope({ admin: true, support: true, common: false }),
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

router.get(ENDPOINT,
    (ctx, next) =>
        graphqlKoa({
            schema: schemas, context: ctx,
            formatError: error => ({
                message: error.message,
                errorMessage: error.originalError && error.originalError.errorMessage,
                locations: error.locations,
                path: error.path,
            }),
        })(ctx, next));

if (process.env.ENABLE_GRAPHIQL) {
    router.get("/cities/graphiql", graphiqlKoa({ endpointURL: ENDPOINT }));
}

app.use(koaBody())
    .use(responseHandler)
    .use(router.routes())
    .use(router.allowedMethods());

export const citiesServer = serverless(app);
