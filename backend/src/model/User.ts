import { BaseModel } from "./BaseModel";

export class User extends BaseModel {
    name: string;
    username: string;
    password: string;
    email: string;
    role: string;
}
