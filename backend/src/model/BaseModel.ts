
export class BaseModel {
    id: string;
    createdAt: number;
    deleted: boolean;
}
