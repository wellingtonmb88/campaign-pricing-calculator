
import { IDataSource } from "../repository/IDataSource";
import { User } from "../model/User";
import * as errorConstants from "./error-constants";
import * as errorHandler from "./error-handler";

const passwordGenerator = require("generate-password");
const bcrypt = require("bcryptjs");
const Boom = require("boom");

/**
 * Find user from db by its username.
 * @param dataStore - obejct that implements interface IDataSource<User>.
 * @param username - string with user"s username.
 */
const getUser = async (dataStore: IDataSource<User>, username: string) => {
    return new Promise<User>(async (resolve, reject) => {
        const result = await dataStore.get("username", username);
        if (result.data) {
            resolve(result.data);
        } else {
            reject(Boom.unauthorized(errorConstants.message.error_incorrect_username));
        }
    });
};

/**
 * Compare password.
 * @param password - string with user"s password.
 * @param passwordEncrypted - string with user"s encrypted password.
 */
export const bcryptCompare = async (password: string, passwordEncrypted: string) => {
    return new Promise((resolve, reject) => {
        return bcrypt.compare(password, passwordEncrypted, (err, isValid) => {
            if (isValid) {
                resolve(true);
            } else {
                reject(Boom.unauthorized(errorConstants.message.error_incorrect_password));
            }
        });
    });
};

/**
 * Encrypt password.
 * @param password - string with user"s password.
 */
export const hashPassword = async (password: string) => {
    return new Promise<string>((resolve, reject) => {
        // Generate a salt at level 10 strength
        return bcrypt.genSalt(10, (error, salt) => {
            bcrypt.hash(password, salt, (err, hash) => {
                if (err) {
                    reject(Boom.serverUnavailable(errorConstants.message.error_password_encryption));
                } else {
                    resolve(hash);
                }
            });
        });
    });
};

/**
 * Verify if user"s credentials is valid.
 * @param dataStore - obejct that implements interface IDataSource<User>.
 * @param password - string with user"s password.
 * @param username - string with user"s username.
 */
export const verifyDefaultCredentials =
    async (dataStore: IDataSource<User>, password: string, username: string) => {
        try {
            const user = await getUser(dataStore, username);
            const isCredentialsValid = await bcryptCompare(password, user.password);
            return isCredentialsValid ? user : null;
        } catch (e) {
            throw e;
        }
    };

/**
 * Verify if user exists in the permanent and temporary database.
 * @param dataStore - obejct dataStore that manages permanet database.
 */
export const verifyIfUserNotExist = async (ctx, dataStore: IDataSource<User>) => {
    const email = ctx.request.body.email;
    const username = ctx.request.body.username;
    const resultEmail = await dataStore.get("email", email);
    const resultUsername = await dataStore.get("username", username);
    if ((resultEmail && resultEmail.data && resultEmail.data.email) ||
        (resultUsername && resultUsername.data && resultUsername.data.username)) {
        errorHandler.sendError(ctx, errorConstants.message.error_user_already_exists, 401);
        return false;
    } else {
        return true;
    }
};
