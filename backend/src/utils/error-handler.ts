
import { GraphQLError } from "graphql";

class ValidationError extends Error {
    errorMessage: string;
    constructor(msg: string, status: number) {
        super(msg);
        this.errorMessage = JSON.stringify({
            code: status,
            message: msg,
        });
    }
}

export const sendError = (ctx, message: string, status: number) => {
    ctx.status = status;
    throw new ValidationError(message, status);
};
