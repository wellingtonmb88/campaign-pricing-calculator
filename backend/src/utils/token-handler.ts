
import { User } from "../model/User";
import * as errorHandler from "./error-handler";

const jwt = require("jsonwebtoken");
const Boom = require("boom");
const secret = process.env.JWT_SECRET_KEY;

/**
 * Create token by user"s properties.
 * @param user - user object.
 */
export const createToken = user => {
    // Sign the JWT
    return jwt.sign(
        {
            id: user._id,
            username: user.username,
            scope: user.roles,
        },
        secret,
        {
            algorithm: "HS256",
            expiresIn: "1h",
        },
    );
};

export const decodeToken = token => {
    return jwt.verify(token, secret);
};

/**
 * Get decoded token from header.
 * @param ctx - koa context
 */
export const getDecodedToken = async (ctx) => {
    const authorization: string = ctx.request.header.authorization;
    if (!authorization || authorization === undefined
        || authorization.substring(0, 7).trim() !== "Bearer") {
        ctx.throw(Boom.unauthorized("Bearer must be provided!"));
        return;
    }
    const token = authorization.split("Bearer ")[1];
    if (!token) {
        ctx.throw(Boom.unauthorized("Token must be provided!"));
        return;
    }
    try {
        return await decodeToken(token);
    } catch (err) {
        ctx.throw(Boom.unauthorized(err.message));
    }
};

/**
 * Generate a token.
 * @param user - user to be added inside the token.
 */
export const generateToken = (user: User) => {
    const token = createToken({
        _id: user.id,
        username: user.username,
        roles: user.role,
    });
    return token;
};
