
const Boom = require("boom");
import { User } from "../model/User";
import { IDataSource } from "../repository/IDataSource";
import * as  credentialsHandler from "../utils/credentials-handler";
import * as  tokenHandler from "../utils/token-handler";
import * as  errorHandler from "../utils/error-handler";

/**
 * Authorize an user by default credentials.
 * @param roles - roles to be authorized.
 * @param dataStore - dataStore that implements interface IDataStore<T>.
 * @param password - string user's password.
 * @param username - string user's username.
 */
export const authorizeDefaultCredentials =
    async (dataStore: IDataSource<User>, password: string, username: string) => {
        let body;
        let status;
        try {
            const user = await credentialsHandler
                .verifyDefaultCredentials(dataStore, password, username);
            status = 202;
            body = tokenHandler.generateToken(user);
        } catch (e) {
            let error;
            if (e.output) {
                status = e.output.statusCode;
                error = e.output.payload;
            } else {
                const boom = Boom.internal();
                status = boom.output.statusCode;
                error = boom.output.payload;
            }
            body = JSON.stringify(error);
        }
        return {
            body,
            status,
        };
    };

/**
 * Verify if a token has scopes admin or support or common.
 * @param admin - scope admin to be validated.
 * @param support - scope support to be validated.
 * @param common - scope common to be validated.
 */
export const authScope = ({ admin, support, common }) => async (ctx, next) => {

    ctx.tokenDecoded = await tokenHandler.getDecodedToken(ctx);
    if (!ctx.tokenDecoded) {
        return;
    }
    const scopes = ctx.tokenDecoded.scope;

    let errorScope;
    let showError = false;
    let hasAtLeastOneScope = false;
    if (admin && !hasAtLeastOneScope) {
        if (scopes === "admin") {
            showError = false;
            hasAtLeastOneScope = true;
        } else {
            showError = true;
            errorScope = "Admin";
        }
    }
    if (support && !hasAtLeastOneScope) {
        if (scopes === "support") {
            showError = false;
            hasAtLeastOneScope = true;
        } else {
            showError = true;
            errorScope = errorScope ? errorScope + " or Support" : "Support";
        }
    }
    if (common && !hasAtLeastOneScope) {
        if (scopes === "common") {
            showError = false;
            hasAtLeastOneScope = true;
        } else {
            showError = true;
            errorScope = errorScope ? errorScope + " or Common" : "Common";
        }
    }
    if (errorScope && showError && !hasAtLeastOneScope) {
        ctx.throw(Boom.unauthorized(errorScope + " scope is required."));
    } else {
        await next();
    }
};
