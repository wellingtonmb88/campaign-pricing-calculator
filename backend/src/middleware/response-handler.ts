'use strict';

/**
 * Formatting error responses.
 * @param ctx - koa context.
 * @param next - koa next function.
 */
export const responseHandler = async (ctx, next) => {
    try {
        ctx.type = 'application/json';
        await next();
    } catch (_error) {
        let error;
        let status;
        if (_error.output) {
            //Error from framework Boom
            status = _error.output.statusCode;
            error =  _error.output.payload ;
        } else {
            //Generic Error
            status = _error.statusCode || _error.status || 500;
            error = { error: _error.message };
        }
        ctx.status = status;
        ctx.body = JSON.stringify(error);
    }
};