
import { Handler } from "aws-lambda";

import { printingsServer } from "./src/api/printings/routers";
import { industriesServer } from "./src/api/industries/routers";
import { audiencesServer } from "./src/api/audiences/routers";
import { citiesServer } from "./src/api/cities/routers";
import { authenticationsServer } from "./src/api/authentications/routers";
import { calculationsServer } from "./src/api/calculations/routers";

export const printings: Handler = printingsServer;
export const industries: Handler = industriesServer;
export const audiences: Handler = audiencesServer;
export const cities: Handler = citiesServer;
export const authentications: Handler = authenticationsServer;
export const calculations: Handler = calculationsServer;
