import * as React from 'react';
import './App.css';
import HomeAppBar from './components/HomeAppBar';
import PricingScreen from "./screens/PricingScreen"
import { StoreContext, IStoreState } from "./context";
import { getCities, deleteCity, createCity, updateCity } from "./services/api/CitiesGraphqlApi";
import { ICity } from './components/forms/CityForm';

class App extends React.Component<{}, IStoreState> {

  constructor() {
    super({})
    this.state = {
      logged: false,
      role: '',
      token: '',
      cities: []
    };
  };

  async componentDidMount() {
    const cities = await getCities();
    this.setState({ cities: cities.filter((city: any) => city.deleted === false) });
  };

  updateLoggedUser = (logged: boolean, role: string, token: string) => {
    this.setState({
      logged, role, token
    });
  };

  saveCity = async (city: ICity) => {
    if (city.id.length > 5) {
      this.updateCity(city);
      this.updateCitiesState(city);
    } else {
      this.createCity(city);
    }
  };

  deleteCity = async (city: ICity) => {
    if (city.id) {
      await deleteCity(city.id, this.state.token);
      this.updateCitiesState(city)
    }
  };

  createCity = async (city: ICity) => {
    const newCity = await createCity(this.getCityJSONObject(city), this.state.token);
    const cities = this.state.cities;
    city.id = newCity.id;
    cities.push(city);
    this.setState({ cities });
  };

  updateCity = async (city: ICity) => {
    await updateCity(this.getCityJSONObject(city), this.state.token);
  };

  getCityJSONObject = (city: any) => {
    return {
      id: city.id,
      name: city.name ? city.name : "",
      distribution: {
        fliersPerMission: city.distribution.fliersPerMission ?
          city.distribution.fliersPerMission : 0,
        costPerMission: city.distribution.costPerMission ? city.distribution.costPerMission : 0,
      },
      purpose: {
        test: city.purpose.test ? city.purpose.test : 0,
        validate: city.purpose.validate ? city.purpose.validate : 0,
        growth: city.purpose.growth ? city.purpose.growth : 0,
        scaleUp: city.purpose.scaleUp ? city.purpose.scaleUp : 0
      },
      lat: city.lat ? city.lat : 0,
      lng: city.lng ? city.lng : 0
    }
  };

  updateCitiesState = (city: any) => {
    const cities = this.state.cities.map((c: ICity) => {
      if (c.id === city.id || c.name === city.name) {
        c.name = city.name;
        c.lat = city.lat;
        c.lng = city.lng;
        c.distribution = city.distribution;
        c.purpose = city.purpose;
        c.deleted = city.deleted;
      }
      return c;
    });
    this.setState({ cities });
  };

  public render() {
    return (
      <StoreContext.Provider value={this.state}>
        <div>
          <HomeAppBar
            updateLoggedUser={this.updateLoggedUser}
            saveCity={this.saveCity}
            deleteCity={this.deleteCity} />
          <PricingScreen />
        </div>
      </StoreContext.Provider>
    )
  }
};

export default App;
