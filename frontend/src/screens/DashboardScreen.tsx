import * as React from 'react';
import Dialog, {
    DialogContent,
} from 'material-ui/Dialog';

import Button from 'material-ui/Button';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from 'material-ui/transitions/Slide';
import CustomList, { IListItem } from '../components/CustomList';
import * as ServiceApiMock from '../services/api/ServiceApiMock';
import { StoreContext, IStoreState } from '../context';

const Transition = (props: any) => {
    return <Slide direction="up" {...props} />;
};

interface IDashboardProps {
    handleOpenClose: boolean;
    handleClose: () => void;
    handleEditCity: (city: IListItem | null) => void;
    handleEditIndustry: (industry: IListItem) => void;
    handleEditPrinting: (printing: IListItem) => void;
    handleDeleteCity: (city: IListItem) => void;
};

class DashboardScreen extends React.Component<IDashboardProps> {

    constructor(props: IDashboardProps) {
        super(props);
    };

    state = {
        cities: [],
        industries: [],
        printings: []
    };

    async componentDidMount() {

        const industries = await ServiceApiMock.getIndustries();
        const printings = await ServiceApiMock.getPrintings();

        this.setState({ industries }, () => {
            this.formatIndustryList();
        });
        this.setState({ printings }, () => {
            this.formatPrintingList();
        });
    };

    formatCityList = (cities: any[]): IListItem[] => {
        return cities.filter((city: any) => city.deleted === false)
            .map((city: any) => {
                return {
                    key: city.id,
                    primaryText: city.name,
                    secondaryText: ""
                }
            })
    };

    formatIndustryList = (): IListItem[] => {
        return this.state.industries.map((industry: any) => {
            return {
                key: industry.id,
                primaryText: `${industry.name}`,
                secondaryText: `Conversion rate: ${industry.cvr}%`
            }
        })
    };

    formatPrintingList = (): IListItem[] => {
        return this.state.printings.map((printing: any) => {
            return {
                key: printing.id,
                primaryText: `Flyers Ordered up to: ${printing.orders}`,
                secondaryText: `Rate: $${printing.rate}`
            }
        })
    };

    openLogin = () => {
        this.setState({ openLogin: true });
    };

    handleLogin = () => {
        this.setState({ openLogin: true });
    };

    closeLogin = () => {
        this.setState({ openLogin: false });
    };

    openCity = () => {
        this.setState({ openCity: true });
    };

    closeCity = () => {
        this.setState({ openCity: false });
    };

    handleEditCity = (cities: any[], city: IListItem | null) => {
        if (city) {
            let editCity = cities.filter((c: any) => c.id === city.key)[0];
            editCity = {
                ...editCity,
                lat: editCity.latitude,
                lng: editCity.longitude
            }
            this.props.handleEditCity(editCity);
        } else {
            this.props.handleEditCity(null);
        }
    };

    handleDeleteCity = (cities: any[], city: IListItem) => {
        const editCity = cities.filter((c: any) => c.id === city.key)[0];
        editCity.deleted = true;
        this.props.handleDeleteCity(editCity);
    };

    handleEditIndustry = (industry: IListItem) => {
        const editIndustry = this.state.industries.filter((i: any) => i.id === industry.key)[0];
        this.props.handleEditIndustry(editIndustry);
    };

    handleDeleteIndustry = (industry: IListItem) => {
        console.log("handleDeleteIndustry", industry)
    };

    handleEditPrinting = (printing: IListItem) => {
        const editPrinting = this.state.printings.filter((p: any) => p.id === printing.key)[0];
        this.props.handleEditPrinting(editPrinting);
    };

    handleDeletePrinting = (printing: IListItem) => {
        console.log("handleDeletePrinting", printing)
    };

    public render() {
        return (
            <StoreContext.Consumer>{(store: IStoreState) =>
                <div>
                    <Dialog
                        fullScreen={true}
                        open={this.props.handleOpenClose}
                        onClose={this.props.handleClose}
                        aria-labelledby="form-dialog-title"
                        transition={Transition}
                    >
                        <AppBar position="static">
                            <Toolbar>
                                <IconButton color="inherit" onClick={this.props.handleClose} aria-label="Close">
                                    <CloseIcon />
                                </IconButton>
                                <Typography variant="title" color="inherit" style={{ flex: 1 }}>Dashboard</Typography>
                            </Toolbar>
                        </AppBar>
                        <DialogContent>

                            <CustomList
                                title={"Cities"}
                                elements={this.formatCityList(store.cities)}
                                handleDelete={(city: IListItem) => { this.handleDeleteCity(store.cities, city) }}
                                handleEdit={(city: IListItem) => { this.handleEditCity(store.cities, city) }} />
                            <Button color="primary" onClick={() => { this.handleEditCity(store.cities, null) }}>Create City</Button>

                            <CustomList
                                title={"Industries"}
                                elements={this.formatIndustryList()}
                                handleDelete={this.handleDeleteIndustry}
                                handleEdit={this.handleEditIndustry} />

                            <CustomList
                                title={"Printings"}
                                elements={this.formatPrintingList()}
                                handleDelete={this.handleDeletePrinting}
                                handleEdit={this.handleEditPrinting} />
                        </DialogContent>
                    </Dialog>
                </div>
            }</StoreContext.Consumer>
        );
    }
};

export default DashboardScreen;
