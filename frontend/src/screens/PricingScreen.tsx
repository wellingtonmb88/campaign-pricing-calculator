import * as React from 'react';

import SimpleSelector from '../components/SimpleSelector';
import CustomMap, { ICity, iconRed } from '../components/CustomMap';
import { StoreContext, IStoreState } from '../context';
import { calculate } from '../services/api/CalculationsGraphql';
import * as ServiceApiMock from '../services/api/ServiceApiMock';


const purposes = [
    "Test", "Growth", "Validate", "ScaleUp"
];

class PricingScreen extends React.Component {

    state = {
        openLogin: false,
        openSignIn: false,
        openCity: false,
        industries: [],
        printings: [],
        refreshMap: true,
        totalCampaign: '',
        costPerAcquisition: '',
        numberOfAcquisitions: '',
        purposeSelected: '',
        industrySelected: '',
        printingSelected: {},
        selectedCities: []
    };

    async componentDidMount() {
        const industries = await ServiceApiMock.getIndustries();
        const printings = await ServiceApiMock.getPrintings();
        this.setState({ industries });
        this.setState({ printings });
    };

    formatIndustriesList = (industries: any[]) => {
        return industries.filter((industry: any) => industry.deleted === false)
            .map((industry: any) => {
                return industry.name;
            });
    };

    formatPrintingsList = (printings: any[]) => {
        return printings.filter((printing: any) => printing.deleted === false)
            .map((printing: any) => {
                return printing.orders.toString();

            });
    };

    formatCityList = (cities: any[]): ICity[] => {
        return cities.filter((city: any) => city.deleted === false)
            .map((city: any) => {
                return {
                    key: city.id,
                    name: city.name,
                    lat: city.latitude,
                    lng: city.longitude,
                    selected: city.selected ? city.selected : false,
                    iconUrl: iconRed
                }
            });
    };

    handleMakerClick = (store: any, city: any) => {
        const sCities: any[] = store.cities.map((c: any) => {
            if (c.id === city.id || c.name === city.name) {
                c.selected = city.selected;
            }
            return c;
        });
        this.setState({ selectedCities: sCities.filter((c: any) => c.selected === true) },
            () => { this.handleCalculation(store.token); });
    };

    formatCityCalculation = () => {
        return this.state.selectedCities.map((city: any) => {
            return {
                distribution: {
                    fliersPerMission: city.distribution.fliersPerMission,
                    costPerMission: city.distribution.costPerMission
                },
                purpose: this.getPurposeFromCity(city, this.state.purposeSelected)
            }
        })
    }

    getPurposeFromCity = (city: any, purpose: string) => {
        switch (purpose.toLowerCase()) {
            case 'test':
                return city.purpose.test;
            case 'validate':
                return city.purpose.validate;
            case 'growth':
                return city.purpose.growth;
            case 'scaleUp':
                return city.purpose.scaleUp;
            default:
                return city.purpose.test;
        }
    };

    handleCalculation = async (token: string) => {
        const {
            purposeSelected,
            industrySelected,
            printingSelected,
            selectedCities
        } = this.state;

        if (selectedCities.length > 0 &&
            purposeSelected &&
            industrySelected &&
            printingSelected) {

            const industry: any = this.state.industries.filter((i: any) =>
                i.name.toLowerCase() === industrySelected.toLowerCase())[0];

            const printing: any = this.state.printings.filter((p: any) =>
                p.orders.toString() === printingSelected)[0];

            const citiesFormatted = this.formatCityCalculation()
            const result = await calculate(
                citiesFormatted,
                printing,
                industry.cvr, token
            );
            if (result) {
                this.setState({
                    totalCampaign: `$${parseFloat(result.totalCampaign.toString()).toFixed(2)}`,
                    costPerAcquisition: `$${parseFloat(result.costPerAcquisition.toString()).toFixed(2)}`,
                    numberOfAcquisitions: `# ${result.numberOfAcquisitions}`
                });
            } else {
                this.setState({
                    totalCampaign: ``,
                    costPerAcquisition: ``,
                    numberOfAcquisitions: ``
                });
            }
        } else {
            this.setState({
                totalCampaign: ``,
                costPerAcquisition: ``,
                numberOfAcquisitions: ``
            });
        }
    };

    public render() {
        const {
            totalCampaign,
            costPerAcquisition,
            numberOfAcquisitions
        } = this.state;

        return (
            <StoreContext.Consumer>{(store: IStoreState) =>
                <div>
                    <CustomMap
                        handleMakerClick={(city: any) => {
                            this.handleMakerClick(store, city);
                        }}
                        cities={this.formatCityList(store.cities)}
                    />
                    <div style={{ display: 'flex', marginTop: 10 }}>
                        <div style={{ width: '50%', textAlign: 'center' }}>

                            <SimpleSelector
                                inputLabel="Purpose"
                                items={purposes}
                                handleChange={(value) => {
                                    this.setState({ purposeSelected: value },
                                        () => { this.handleCalculation(store.token); });
                                }}
                            />

                            <SimpleSelector
                                inputLabel="Industry"
                                items={this.formatIndustriesList(this.state.industries)}
                                handleChange={(value) => {
                                    this.setState({ industrySelected: value },
                                        () => { this.handleCalculation(store.token); });
                                }}
                            />
                            <SimpleSelector
                                inputLabel="Printing"
                                items={this.formatPrintingsList(this.state.printings)}
                                handleChange={(value) => {
                                    this.setState({ printingSelected: value },
                                        () => { this.handleCalculation(store.token); });
                                }}
                            />
                        </div>

                        <div style={{ width: '50%' }}>
                            <p >Total Campaign</p>
                            {totalCampaign}
                        </div>
                        <div style={{ width: '50%' }}>
                            <p >Cost per Acquisition</p>
                            {costPerAcquisition}
                        </div>
                        <div style={{ width: '50%' }}>
                            <p >Number of acquisitions</p>
                            {numberOfAcquisitions}
                        </div>
                    </div>
                </div>
            }</StoreContext.Consumer>
        );
    }
};

export default PricingScreen;
