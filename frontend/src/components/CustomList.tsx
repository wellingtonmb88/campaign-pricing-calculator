import * as React from 'react';

import List, {
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText,
} from 'material-ui/List';

import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AlertDialog from './AlertDialog';

export interface IListItem {
    key: string;
    primaryText: string;
    secondaryText: string;
};

interface IProps {
    title: string;
    elements: IListItem[];
    handleDelete: (element: IListItem) => void;
    handleEdit: (element: IListItem) => void;
};

interface IState {
    openAlertDialog: boolean;
    element: IListItem | null | undefined;
};

class CustomList extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            openAlertDialog: false,
            element: null
        }
    };

    openAlertDialog = (element: IListItem) => {
        this.setState({ openAlertDialog: true, element });
    };
    closeAlertDialog = () => {
        this.setState({ openAlertDialog: false, element: null });
    };
    onDelete = () => {
        const { element } = this.state;
        if (element) {
            this.props.handleDelete(element);
        }
        this.closeAlertDialog();
    };

    render() {
        const { elements, title, handleEdit } = this.props;
        return (
            <div>
                <h2>{title}</h2>
                {elements.length > 0 ?
                    (<div style={{
                        maxHeight: 380,
                        overflow: 'auto'
                    }}>
                        <AlertDialog
                            title="Are you sure to delete that item?"
                            contentText=""
                            handleOpenClose={this.state.openAlertDialog}
                            handleAgree={this.onDelete}
                            handleClose={this.closeAlertDialog}
                        />
                        <List>
                            {elements.map(element => {
                                return <ListItem key={element.key}>
                                    <ListItemAvatar >
                                        <Avatar>
                                            <EditIcon onClick={() => { handleEdit(element); }} />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={element.primaryText}
                                        secondary={element.secondaryText ? element.secondaryText : null}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton aria-label="Delete" onClick={() => { this.openAlertDialog(element); }}>
                                            <DeleteIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            })}
                        </List>
                    </div>) : ("Empty List")}
            </div>
        )
    }
};

export default CustomList;