import * as React from 'react';

interface IProps {
    children: any;
    test: boolean;
};

export default class If extends React.Component<IProps> {
    constructor(props: IProps) {
        super(props);
    }
    render() {
        const { test, children } = this.props;
        return (
            <div>
                {test ? children : false}
            </div>
        )
    }
};