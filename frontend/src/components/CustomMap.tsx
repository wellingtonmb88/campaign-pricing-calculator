import * as React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import { compose, withProps, withStateHandlers, withState, withHandlers } from "recompose";
import Button from 'material-ui/Button';
import List, {
    ListItem,
} from 'material-ui/List';
import If from '../components/If';

export interface ICity {
    key: any;
    name: string;
    lat: number;
    lng: number;
    selected: boolean;
    iconUrl: string;
};

interface IProps {
    handleMakerClick: (city: any) => void;
    cities: ICity[];
};

interface IRef {
    map: any;
};

export const iconRed = "https://maps.google.com/mapfiles/ms/icons/red.png";
export const iconGreen = "https://maps.google.com/mapfiles/ms/icons/green.png";

const styleCities = {
    height: 380,
    overflow: 'auto',
};

// tslint:disable-next-line:variable-name
const selectCity = (city: ICity, props: any, iProps: IProps) => {
    city.selected ? city.iconUrl = iconRed : city.iconUrl = iconGreen;
    city.selected = !city.selected;
    props.onMarkerClick(city)
    iProps.handleMakerClick(city);
};

// tslint:disable-next-line:variable-name
const createMapComponent = (iProps: IProps) => {
    return compose(
        withProps({
            googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places",
            loadingElement: <div style={{ width: '100%', height: '100%', backgroundColor: 'blue' }} />,
            containerElement: <div style={{ width: '100%', height: '400px', display: 'flex' }} />,
            mapElement: <div style={{ width: '100%', height: '100%' }} />,
        }),
        withScriptjs,
        withGoogleMap,
        withState('zoom', 'onZoomChange', 8),
        withHandlers(() => {
            const refs: IRef = {
                map: undefined,
            }
            return {
                onMapMounted: () => (ref: any) => {
                    refs.map = ref
                },
                onZoomChanged: ({ onZoomChange }) => () => {
                    if (refs && refs.map) {
                        onZoomChange(refs.map.getZoom());
                    }
                }
            }
        }),
        withStateHandlers(() => ({
            key: "-1",
            updateMap: false,
            iconUrl: iconRed,
        }), {
                onMarkerClick: () => (city: ICity) => ({
                    key: city.key,
                    selected: city.selected,
                    iconUrl: city.iconUrl,
                }),

            })
    )((props: any) =>
        <div >
            <GoogleMap
                zoom={props.zoom}
                ref={props.onMapMounted}
                onZoomChanged={props.onZoomChanged}
                defaultCenter={{ lat: -34.397, lng: 150.644 }}>
                {iProps.cities.map(city =>
                    <Marker
                        icon={{ url: city.iconUrl }}
                        key={city.key}
                        position={{ lat: city.lat ? city.lat : 0, lng: city.lng ? city.lng : 0 }}
                        onClick={() => {
                            selectCity(city, props, iProps);
                        }} />
                )}
            </GoogleMap>
            <If test={iProps.cities.length > 0}>
                <List style={styleCities} >
                    {iProps.cities.map(city => {
                        return <ListItem key={city.key} >
                            <Button key={city.key} onClick={() => {
                                selectCity(city, props, iProps);
                            }} color={city.selected ? "primary" : "inherit"}>{city.name}</Button>
                        </ListItem>
                    })}
                </List>
            </If>
        </div>
    );
};

class CustomMap extends React.Component<IProps> {
    constructor(props: IProps) {
        super(props);
    }

    shouldComponentUpdate(nextProps: any, nextState: any) {
        if (this.props.cities.length === nextProps.cities.length) {
            return false
        } else {
            return true
        }
    };

    render() {
        const Map = createMapComponent(this.props);
        return (
            <Map />
        )
    }
};

export default CustomMap;