import * as React from 'react';
import Button from 'material-ui/Button';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';

interface IProps {
    title: string;
    contentText: string;
    handleOpenClose: boolean;
    handleClose: () => void;
    handleAgree: () => void;
};

class AlertDialog extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    };

    render() {
        const { title, contentText, handleAgree, handleClose, handleOpenClose } = this.props;
        return (
            <div>
                <Dialog
                    open={handleOpenClose}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">{contentText}</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">CANCEL</Button>
                        <Button onClick={handleAgree} color="primary" autoFocus>OK</Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
};

export default AlertDialog;