import * as React from 'react';

import { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';

interface IProps {
    items: string[];
    inputLabel: string;
    handleChange: (value: string) => void;
};

class SimpleSelector extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    };

    state = {
        value: '',
    };

    handleChange = (event: any) => {
        this.setState({ [event.target.name]: event.target.value });
        this.props.handleChange(event.target.value);
    };

    render() {
        const { items, inputLabel } = this.props;
        return (
            <form autoComplete="off">
                <FormControl style={{
                    margin: 10,
                    minWidth: 100
                }}>
                    <InputLabel htmlFor={`input-label-${inputLabel}`}>{inputLabel}</InputLabel>
                    <Select
                        value={this.state.value}
                        onChange={this.handleChange}
                        inputProps={{
                            name: 'value',
                            id: `select-id-${inputLabel}`,
                        }}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {items.map(item => {
                            return <MenuItem
                                key={item.toLowerCase()}
                                value={item.toLowerCase()}>{item}</MenuItem>
                        })}
                    </Select>
                </FormControl>
            </form>
        );
    }
};

export default SimpleSelector;