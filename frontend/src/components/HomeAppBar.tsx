import * as React from 'react';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

import LoginForm from '../components/forms/LoginForm';
import CityForm from '../components/forms/CityForm';
import DashboardScreen from '../screens/DashboardScreen';
import If from '../components/If';
import { StoreContext, IStoreState } from "../context";
import { ICity } from './forms/CityForm';
import { authentication } from '../services/api/AuthenticationsGraphql'

const style = {
    root: {
        flexGrow: 1,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    }
};

interface IProps {
    updateLoggedUser: (logged: boolean, role: string, token: string) => void;
    saveCity: (city: ICity) => void;
    deleteCity: (city: ICity) => void;
};

class HomeAppBar extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    };

    state = {
        showErrorLogin: false,
        openLogin: false,
        openSignIn: false,
        openCity: false,
        openIndustry: false,
        openPrinting: false,
        openDashboard: false,
        cities: [],
        cityToEdit: undefined,
        industryToEdit: null,
        printingToEdit: null
    };

    handleLogin = async (username: string, password: string) => {
        const auth = await authentication(username, password);
        if (auth) {
            this.setState({ openLogin: false, showErrorLogin: false });
            this.props.updateLoggedUser(true, auth.role, auth.token);
        } else {
            this.setState({ showErrorLogin: true });
        }
    };

    handleLoginOut = () => {
        this.props.updateLoggedUser(false, '', '');
    };

    openLogin = () => {
        this.setState({ openLogin: true });
    };

    closeLogin = () => {
        this.setState({ openLogin: false });
    };

    openCity = () => {
        this.setState({ openCity: true });
    };

    closeCity = () => {
        this.setState({ openCity: false, cityToEdit: null });
    };

    saveCity = (city: ICity) => {
        this.setState({ openCity: false, cityToEdit: null });
        this.props.saveCity(city);
    };

    openDashboard = () => {
        this.setState({ openDashboard: true });
    };

    closeDashboard = () => {
        this.setState({ openDashboard: false });
    };

    handleEditCity = (city: any) => {
        this.setState({ openCity: true, cityToEdit: city });
    };

    handleDeleteCity = (city: any) => {
        this.props.deleteCity(city);
    };

    handleEditIndustry = (industry: any) => {
        this.setState({ openIndustry: true, industryToEdit: industry });
    };

    handleEditPrinting = (printing: any) => {
        this.setState({ openPrinting: true, printingToEdit: printing });
    };

    public render() {
        const { cityToEdit } = this.state;
        return (
            <StoreContext.Consumer>{(store: IStoreState) =>
                <div style={style.root}>
                    <AppBar position="static">
                        <Toolbar>
                            <Typography variant="title" color="inherit" style={style.flex}>Pricing</Typography>
                            <If test={!store.logged}>
                                <Button color="inherit" onClick={this.openLogin}>Login</Button>
                            </If>
                            <If test={store.logged}>
                                <Button color="inherit" onClick={this.openDashboard}>Dashboard</Button>
                                <Button color="inherit" onClick={this.handleLoginOut}>Logout</Button>
                            </If>
                        </Toolbar>
                    </AppBar>
                    <LoginForm
                        showError={this.state.showErrorLogin}
                        handleOpenClose={this.state.openLogin}
                        handleClose={this.closeLogin}
                        handleLogin={this.handleLogin}
                        openSign={() => { }}
                    />

                    <CityForm
                        city={cityToEdit ? cityToEdit : null}
                        handleOpenClose={this.state.openCity}
                        handleClose={this.closeCity}
                        handleSave={this.saveCity} />

                    <DashboardScreen
                        handleOpenClose={this.state.openDashboard}
                        handleClose={this.closeDashboard}
                        handleEditCity={this.handleEditCity}
                        handleDeleteCity={this.handleDeleteCity}
                        handleEditIndustry={this.handleEditIndustry}
                        handleEditPrinting={this.handleEditPrinting} />
                </div>
            }</StoreContext.Consumer>
        )
    }
};

export default HomeAppBar;