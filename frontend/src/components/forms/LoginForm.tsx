import * as React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogTitle,
} from 'material-ui/Dialog';
import { StoreContext, IStoreState } from "../../context";
import If from '../If';

interface IProps {
    showError: boolean;
    handleOpenClose: boolean;
    handleClose: () => void;
    handleLogin: (username: string, password: string) => void;
    openSign: () => void;
};

class LoginForm extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    };

    state = {
        username: '',
        password: '',
        disableLoginButton: true
    };

    handleChange = (event: any) => {
        this.setState({ [event.target.id]: event.target.value },
            () => {
                const { username, password } = this.state;
                if (username.length > 4 && password.length > 4) {
                    this.setState({ disableLoginButton: false });
                } else {
                    this.setState({ disableLoginButton: true });
                }
            }
        );
    };

    handleLogin = () => {
        const { username, password } = this.state;
        this.props.handleLogin(username, password);
    };

    render() {
        const { disableLoginButton } = this.state;
        return (
            <StoreContext.Consumer>{(store: IStoreState) =>
                <div>
                    <Dialog
                        open={this.props.handleOpenClose}
                        onClose={this.props.handleClose}
                        aria-labelledby="form-dialog-title"
                    >
                        <DialogTitle id="form-dialog-title">Login</DialogTitle>
                        <DialogContent>
                            <TextField
                                required={true}
                                autoFocus={true}
                                onChange={this.handleChange}
                                margin="dense"
                                id="username"
                                value={this.state.username}
                                label="Enter your username"
                                type="string"
                                fullWidth={true}
                            />
                            <TextField
                                required={true}
                                autoFocus={false}
                                onChange={this.handleChange}
                                value={this.state.password}
                                margin="dense"
                                id="password"
                                label="Enter your password"
                                type="password"
                                fullWidth={true}
                            />
                            <If test={this.props.showError}>
                                <p style={{ color: 'red' }}>Invalid authentication</p>
                            </If>
                        </DialogContent>
                        <DialogActions>
                            <If test={store.logged && store.role === 'admin'}>
                                <Button onClick={this.props.openSign} color="primary">SignIn</Button>
                            </If>
                            <Button
                                disabled={disableLoginButton}
                                onClick={this.handleLogin} color="primary">Login</Button>
                        </DialogActions>
                    </Dialog>
                </div>
            }</StoreContext.Consumer>
        );
    }
};

export default LoginForm;