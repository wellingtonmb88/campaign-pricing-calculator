import * as React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogContent,
} from 'material-ui/Dialog';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from 'material-ui/transitions/Slide';
import Radio, { RadioGroup } from 'material-ui/Radio';
import { FormLabel, FormControl, FormControlLabel } from 'material-ui/Form';

const styles = {
    flex: {
        flex: 1,
    },
};

interface IProps {
    handleOpenClose: boolean;
    handleClose: () => void;
    handleSave: () => void;
}

const Transition = (props: any) => {
    return <Slide direction="up" {...props} />;
}

class CreateAccountForm extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    }
    state = {
        value: 'female',
    };

    handleChange = (event: any) => {
        this.setState({ value: event.target.value });
    };

    render() {
        return (
            <div>
                <Dialog
                    fullScreen={true}
                    open={this.props.handleOpenClose}
                    onClose={this.props.handleClose}
                    aria-labelledby="form-dialog-title"
                    transition={Transition}
                >

                    <AppBar position="static">
                        <Toolbar>
                            <IconButton color="inherit" onClick={this.props.handleClose} aria-label="Close">
                                <CloseIcon />
                            </IconButton>
                            <Typography variant="title" color="inherit" style={styles.flex}>SignIn</Typography>
                            <Button onClick={this.props.handleSave} color="inherit">Save</Button>
                        </Toolbar>
                    </AppBar>
                    <DialogContent>
                        <TextField
                            required={true}
                            autoFocus={true}
                            margin="dense"
                            id="name"
                            label="Enter your name"
                            type="string"
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={true}
                            margin="dense"
                            id="username"
                            label="Enter your username"
                            type="string"
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={true}
                            margin="dense"
                            id="email"
                            label="Enter your email"
                            type="email"
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={true}
                            margin="dense"
                            id="role"
                            label="Enter your role"
                            type="text"
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="password"
                            label="Enter your password"
                            type="password"
                            fullWidth={true}
                        />
                        <FormControl component="fieldset" required>
                            <FormLabel component="legend">Role</FormLabel>
                            <RadioGroup
                                aria-label="role"
                                name="role1"
                                value={this.state.value}
                                onChange={this.handleChange}
                            >
                                <FormControlLabel value="admin" control={<Radio />} label="Admin" />
                                <FormControlLabel value="support" control={<Radio />} label="Support" />
                            </RadioGroup>
                        </FormControl>
                    </DialogContent>
                </Dialog>
            </div>
        );
    }
};

export default CreateAccountForm;