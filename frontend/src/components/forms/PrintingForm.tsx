import * as React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogTitle,
} from 'material-ui/Dialog';

export interface IPrinting {
    orders: number;
    rate: number;
};

interface IProps {
    printing: IPrinting | null | undefined;
    handleOpenClose: boolean;
    handleClose: () => void;
    handleSave: (printing: IPrinting) => void;
};

class PrintingForm extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    };

    state = {
        orders: 0,
        rate: 0
    };

    handleChange = (event: any) => {
        this.setState({ [event.target.id]: event.target.value });
    };

    handleSave = () => {
        const {
            orders,
            rate
        } = this.state;

        const printing: IPrinting = {
            orders,
            rate
        };
        this.props.handleSave(printing);
        this.clearState();
    };

    handleClose = () => {
        this.props.handleClose();
        this.clearState();
    };

    clearState = () => {
        this.setState({
            orders: 0,
            rate: 0
        });
    };

    render() {
        const {
            orders,
            rate
        } = this.state;
        const { printing } = this.props;
        return (
            <div>
                <Dialog
                    open={this.props.handleOpenClose}
                    onClose={this.props.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">{printing ? `Update Printing` : `Create Printing`}</DialogTitle>
                    <DialogContent>
                        <TextField
                            required={true}
                            autoFocus={true}
                            margin="dense"
                            id="orders"
                            label="Enter Printing's orders"
                            type="number"
                            value={orders > 0 ? orders : printing ? printing.orders : 0}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="rate"
                            label="Enter Printing's rate"
                            type="number"
                            value={rate > 0 ? rate : printing ? printing.rate : 0}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleSave} color="primary">Save</Button>
                        <Button onClick={this.handleClose} color="primary">Cancel</Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
};

export default PrintingForm;