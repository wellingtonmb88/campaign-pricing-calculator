import * as React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogContent,
} from 'material-ui/Dialog';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from 'material-ui/transitions/Slide';
import If from '../If';

export interface ICity {
    id: string;
    name: string;
    lat: number;
    lng: number;
    distribution: {
        fliersPerMission: number;
        costPerMission: number;
    };
    purpose: {
        test: number;
        validate: number;
        growth: number;
        scaleUp: number;
    };
    deleted: boolean;
};

interface IProps {
    city: ICity | null | undefined;
    handleOpenClose: boolean;
    handleClose: () => void;
    handleSave: (city: ICity) => void;
};

const Transition = (props: any) => {
    return <Slide direction="up" {...props} />;
};

class SignInForm extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    };

    state = {
        cityId: "",
        name: "",
        lat: 0,
        lng: 0,
        fliersPerMission: 0,
        costPerMission: 0,
        test: 0,
        validate: 0,
        growth: 0,
        scaleUp: 0
    };

    componentWillReceiveProps(nextProps: any) {
        const { city } = nextProps;
        if (city) {
            this.setState({
                cityId: city.id,
                name: city.name,
                lat: city.lat,
                lng: city.lng,
                fliersPerMission: city.distribution.fliersPerMission,
                costPerMission: city.distribution.costPerMission,
                test: city.purpose.test,
                validate: city.purpose.validate,
                growth: city.purpose.growth,
                scaleUp: city.purpose.scaleUp,
                deleted: city.deleted
            });
        }
    };

    handleChange = (event: any) => {
        this.setState({ [event.target.id]: event.target.value });
    };

    handleSave = () => {
        const {
            cityId,
            name,
            lat,
            lng,
            fliersPerMission,
            costPerMission,
            test,
            validate,
            growth,
            scaleUp
        } = this.state;

        const cityToSave: ICity = {
            id: cityId,
            name: name,
            lat: lat,
            lng: lng,
            distribution: {
                fliersPerMission: fliersPerMission,
                costPerMission: costPerMission,
            },
            purpose: {
                test: test,
                validate: validate,
                growth: growth,
                scaleUp: scaleUp,
            },
            deleted: false
        };
        this.props.handleSave(cityToSave);
        this.clearState();
    };

    handleClose = () => {
        this.props.handleClose();
        this.clearState();
    };

    clearState = () => {
        this.setState({
            formFilled: false,
            cityId: "",
            name: "",
            lat: 0,
            lng: 0,
            fliersPerMission: 0,
            costPerMission: 0,
            test: 0,
            validate: 0,
            growth: 0,
            scaleUp: 0
        });
    };

    render() {
        const {
            cityId,
            name,
            lat,
            lng,
            fliersPerMission,
            costPerMission,
            test,
            validate,
            growth,
            scaleUp
        } = this.state;
        const { city } = this.props;
        return (
            <div>
                <Dialog
                    fullScreen={true}
                    open={this.props.handleOpenClose}
                    onClose={this.props.handleClose}
                    aria-labelledby="form-dialog-title"
                    transition={Transition}
                >
                    <AppBar position="static">
                        <Toolbar>
                            <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                                <CloseIcon />
                            </IconButton>
                            <Typography variant="title" color="inherit" style={{ flex: 1 }}>{city ? `Update City` : `Create City`}</Typography>
                            <Button onClick={this.handleSave} color="inherit">Save</Button>
                        </Toolbar>
                    </AppBar>
                    <DialogContent>
                        <If test={false}>
                            <TextField
                                disabled={true}
                                id="cityId"
                                type="string"
                                value={cityId}
                                onChange={this.handleChange}
                            />
                        </If>
                        <TextField
                            required={true}
                            autoFocus={true}
                            margin="dense"
                            id="name"
                            label="Enter city's name"
                            type="string"
                            value={name}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="lat"
                            label="Enter city's latitude"
                            type="string"
                            value={lat}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="lng"
                            label="Enter city's longitude"
                            type="string"
                            value={lng}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <h3>Distribution</h3>
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="fliersPerMission"
                            label="Enter city's fliers per mission"
                            type="number"
                            value={fliersPerMission}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="costPerMission"
                            label="Enter city's cost per mission"
                            type="number"
                            value={costPerMission}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <h3>Purpose</h3>
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="test"
                            label="Enter city's purpose Test"
                            type="number"
                            value={test}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="validate"
                            label="Enter city's purpose Validate"
                            type="number"
                            value={validate}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="growth"
                            label="Enter city's purpose Growth"
                            type="number"
                            value={growth}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="scaleUp"
                            label="Enter city's purpose ScaleUp"
                            type="number"
                            value={scaleUp}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                    </DialogContent>
                </Dialog>
            </div>
        );
    }
};

export default SignInForm;