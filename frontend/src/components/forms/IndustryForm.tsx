import * as React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogTitle,
} from 'material-ui/Dialog';

export interface IIndustry {
    name: string;
    cvr: number;
};

interface IProps {
    industry: IIndustry | null | undefined;
    handleOpenClose: boolean;
    handleClose: () => void;
    handleSave: (industry: IIndustry) => void;
};

class IndustryForm extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    };

    state = {
        name: "",
        cvr: 0
    };

    handleChange = (event: any) => {
        this.setState({ [event.target.id]: event.target.value });
    };

    handleSave = () => {
        const {
            name,
            cvr
        } = this.state;

        const industry: IIndustry = {
            name,
            cvr
        };
        this.props.handleSave(industry);
        this.clearState();
    };

    handleClose = () => {
        this.props.handleClose();
        this.clearState();
    };

    clearState = () => {
        this.setState({
            name: "",
            cvr: 0
        });
    };

    render() {
        const {
            name,
            cvr
        } = this.state;
        const { industry } = this.props;
        return (
            <div>
                <Dialog
                    open={this.props.handleOpenClose}
                    onClose={this.props.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">{industry ? `Update Industry` : `Create Industry`}</DialogTitle>
                    <DialogContent>
                        <TextField
                            required={true}
                            autoFocus={true}
                            margin="dense"
                            id="name"
                            label="Enter Industry's name"
                            type="string"
                            value={name.length > 1 ? name : industry ? industry.name : ""}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                        <TextField
                            required={true}
                            autoFocus={false}
                            margin="dense"
                            id="cvr"
                            label="Enter Industry's conversion rate"
                            type="number"
                            value={cvr > 0 ? cvr : industry ? industry.cvr : 0}
                            onChange={this.handleChange}
                            fullWidth={true}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleSave} color="primary">Save</Button>
                        <Button onClick={this.handleClose} color="primary">Cancel</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
};

export default IndustryForm;