const baseUrl = "http://localhost:3000";
const api = "/calculations/graphql";

export const calculate = async (cities: any[], printing: any, conversionRate: number, token: string) => {

    const formatCities = cities.map(city => {
        return `{
        distribution: {
          fliersPerMission: ${city.distribution.fliersPerMission},
          costPerMission: ${city.distribution.costPerMission}
        },
        purpose: ${city.purpose}
      }`
    });

    return await fetch(`${baseUrl}${api}`,
        {
            method: 'POST',
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify({
                "query": `{
                    calculate(cities:[${formatCities}],
                        printing: {
                            orders: ${printing.orders},
                            rate:${printing.rate}
                        },conversionRate: ${conversionRate})
                        {   
                            totalCampaign,
                            costPerAcquisition,
                            numberOfAcquisitions
                        }
                    }
                `
            })
        })
        .then(res => res.json())
        .then(r => {
            if (!r.data || !r.data.calculate) {
                return null
            }
            const result = r.data.calculate;
            return {
                totalCampaign: result.totalCampaign,
                costPerAcquisition: result.costPerAcquisition,
                numberOfAcquisitions: result.numberOfAcquisitions
            }
        });
}
