const baseUrl = "http://localhost:3000";
const api = "/authentications/graphql";
import * as jwtDecode from 'jwt-decode';

export const authentication = async (username: string, password: string) =>
    await fetch(`${baseUrl}${api}?query={authenticate(username:"${username}", password:"${password}"){token}}`)
        .then(res => res.json())
        .then(result => {
            if (result.data.authenticate === null) {
                return null;
            }
            const token = result.data.authenticate.token;
            if (token === null) {
                return null;
            }
            const decoded: any = jwtDecode(token);
            return {
                role: decoded.scope,
                token
            }
        });