const baseUrl = "http://localhost:3000";
const api = "/cities/graphql";

export const getCities = async () =>
    await fetch(`${baseUrl}${api}?query={cities(page:1, limit:100){count page items{id name latitude longitude 
        distribution{fliersPerMission,costPerMission} purpose{test validate growth scaleUp} deleted}}}`)
        .then(res => res.json())
        .then(result => result.data.cities.items);

export const createCity = async (city: any, token: string) =>
    await fetch(`${baseUrl}${api}`,
        {
            method: 'POST',
            headers: {
                "content-type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                "query": `mutation{createCity(newCity:
                  {
                    name: "${city.name}", 
                    distribution:{
                      fliersPerMission: ${city.distribution.fliersPerMission},
                      costPerMission: ${city.distribution.costPerMission}
                    }, 
                    purpose: {
                      test: ${city.purpose.test},
                      validate: ${city.purpose.validate},
                      growth: ${city.purpose.growth},
                      scaleUp: ${city.purpose.scaleUp}
                    },
                    latitude: ${city.lat},
                    longitude: ${city.lng}
                })
                  {id name deleted}}`
            })
        })
        .then(res => res.json())
        .then(result => result.data.createCity);

export const updateCity = async (city: any, token: string) =>
    await fetch(`${baseUrl}${api}`,
        {
            method: 'POST',
            headers: {
                "content-type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                "query": `mutation{updateCity(city:
                      {
                        id: "${city.id}",
                        name: "${city.name}", 
                        distribution:{
                            fliersPerMission: ${city.distribution.fliersPerMission},
                            costPerMission: ${city.distribution.costPerMission}
                        }, 
                        purpose: {
                            test: ${city.purpose.test},
                            validate: ${city.purpose.validate},
                            growth: ${city.purpose.growth},
                            scaleUp: ${city.purpose.scaleUp}
                        },
                        latitude: ${city.lat},
                        longitude: ${city.lng}
                    })
                      {id name deleted}}`
            })
        })
        .then(res => res.json())
        .then(result => console.log("result", result));

export const deleteCity = async (cityId: string, token: string) =>
    await fetch(`${baseUrl}${api}`,
        {
            method: 'POST',
            headers: {
                "content-type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                "query": `mutation{deleteCity(id:"${cityId}"){id name deleted}}`
            })
        })
        .then(res => res.json())
        .then(result => console.log("result", result));