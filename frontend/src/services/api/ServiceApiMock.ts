
const cities = [
    {
        id: "1",
        name: "Sydney",
        distribution: {
            fliersPerMission: 6000,
            costPerMission: 0.15
        },
        purpose: {
            test: 15000,
            validate: 25000,
            growth: 35000,
            scaleUp: 45000
        },
        lat: -84.397,
        lng: 850.644,
        deleted: false
    },
    {
        id: "2",
        name: "Wellington",
        distribution: {
            fliersPerMission: 7000,
            costPerMission: 0.75
        },
        purpose: {
            test: 75000,
            validate: 55000,
            growth: 65000,
            scaleUp: 95000
        },
        lat: -44.37797,
        lng: 550.644,
        deleted: false
    },
];

const indutries = [
    {
        id: "1",
        name: "Food Delivery",
        cvr: 1.5,
        deleted: false
    },
    {
        id: "2",
        name: "Food in boxes",
        cvr: 0.5,
        deleted: false
    },
    {
        id: "3",
        name: "B2C Services",
        cvr: 0.5,
        deleted: false
    },
    {
        id: "4",
        name: "Ridesharing",
        cvr: 1.5,
        deleted: false
    },
];

const printings = [
    {
        id: "1",
        orders: 6000,
        rate: 0.16,
        deleted: false
    },
    {
        id: "2",
        orders: 7000,
        rate: 0.15,
        deleted: false
    },
    {
        id: "3",
        orders: 8000,
        rate: 0.18,
        deleted: false
    }
];

export const getCities = async () => {
    return cities;
};

export const getIndustries = async () => {
    return indutries;
};

export const getPrintings = async () => {
    return printings;
};


