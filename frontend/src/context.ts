
import * as React from "react";

export interface IStoreState {
    logged: boolean;
    role: string;
    token: string;
    cities: any[];
}

const stateUser: IStoreState = {
    logged: false,
    role: '',
    token: '',
    cities: []
};

export const StoreContext = React.createContext<IStoreState>(stateUser);
